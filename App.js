import React from 'react';
import {AsyncStorage,AppRegistry} from 'react-native';
import getTheme from './native-base-theme/components';  
import * as Permissions from 'expo-permissions'
import { StyleProvider,Root, Spinner } from 'native-base';
import Router from './app/utils/Router';
import CenturyGothic from './assets/fonts/CenturyGothic.ttf';
import {Ionicons, FontAwesome} from '@expo/vector-icons';
import material from './native-base-theme/variables/material';
import * as Font from 'expo-font';
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: true, loggedIn:false };
  }
  async notifications() {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        return;
      }
  }
  startOver(){
    this.notifications();
  }
  
  async componentWillMount() {
    this.startOver();
    await Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      'CenturyGothic':CenturyGothic,
      ...Ionicons.font,
      ...FontAwesome.font,
    });
    try{
      let uid = await AsyncStorage.getItem('uid');
      if(uid){
        this.setState({loggedIn:true})
      }
    }
    catch(error){
      console.log('error getting uid: ', error);
    }
    this.setState({isLoading:false})
  }
  render() {
    return (
      <Root>
      <StyleProvider  style={getTheme(material)}>   
      {
        this.state.isLoading?
          <Spinner />
        :
        <Router loggedIn={ this.state.loggedIn }  />
      }
      </StyleProvider> 
      </Root>
    );
  }
}