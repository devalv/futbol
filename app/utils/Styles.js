'use strict';
var React = require('react-native');
var {StyleSheet,} = React;
import { Platform, Dimensions, PixelRatio } from "react-native";
const platform = Platform.OS;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const isIphoneX = platform === "ios" && (deviceHeight === 812 || deviceWidth === 812);
const headerHeight = platform === "ios" ? (isIphoneX ? 88 : 64) : 56;
module.exports = StyleSheet.create({
    paddingText:{
        margin:15,
    },
    paddingTextCustom:{
        marginVertical:5,
        marginHorizontal:15,
    },
    btnPadd:{
        margin:5,
    },
    whiteSpace:{
        height:100,
        backgroundColor:'transparent',
    },
    ListOfMaps:{
        width: '100%',
        flex: 1,
        height: (deviceHeight- headerHeight)/2,
    },
    ListOfMapsMap:{ 
        width:'100%', 
        flex: 1,
        height: (deviceHeight- headerHeight)/2,
    },
    littleFlex:{
        flex:0.1
    },
    bigFlex:{
        flex:0.8,
        justifyContent:'center',
        alignItems: 'center',
        alignContent: 'center',
    },
    mediumBigFlex:{
        flex:0.6,
        justifyContent:'center',
        alignItems: 'center',
        alignContent: 'center',
    },
    mediumFlex:{
        flex:0.2
    },
    processText:{
        textAlign:'center'
    },
    processButton:{
        flex:0.5,
        width:'50%',
        justifyContent:'center',
        alignItems:'center',
        alignContent:'center',
    },
    mainImageContainer:{
        width:'100%',  
        padding:5,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "stretch",
    },
    mainImageContainerCustom:{
        width:'100%',  
        padding:40,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "stretch",
    },
    customImageContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "stretch",
    },
    customImageContainerV2:{
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "stretch",
        marginVertical: 20,
    },
    textContainer:{
        width:'100%',  
        padding:40,
    },
    centerSpinner:{
        backgroundColor:'#fff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height:deviceHeight
    },
    squareTienda:{
        width:10,
        height:34,
        backgroundColor:'#B2D0EF',
        marginRight:15
      },
      squareDrinks:{
        width:10,
        height:34,
        backgroundColor:'#e4e4dc',
        marginRight:15
      },
      drawerTextOptions:{
        fontSize:16,
        color:'#fff',
        marginLeft: 10,
        marginTop:5,
    },
    drawerContainer:{
        width:'100%',
        minWidth:'100%'

    },
    drawerTop:{
        width:'100%',
        height:150,
    },
    drawerBottom:{
        width:'100%',
        height:deviceHeight-150-headerHeight,
    },
    drawerItemContainer:{
        backgroundColor:'#404441',
        borderTopWidth:1,
        borderTopColor:'#fff',
        flexWrap: 'wrap', 
        alignItems: 'flex-start',
        flexDirection:'row',
        height:34,
    },
    inlineBlock:{
        flexWrap: 'wrap', 
        alignItems: 'flex-start',
        flexDirection:'row',
    },
    inlineBlockCustom:{
        flexWrap: 'wrap', 
        alignItems: 'flex-start',
        flexDirection:'row',
        paddingBottom:7
    },
    listGolesContainer:{
        backgroundColor:'#000',
        width: '100%', 
        marginVertical:0,
        paddingTop:0,
        paddingBottom:0,
        marginLeft: 0, 
        paddingLeft: 0,
        paddingRight: 0, 
        marginRight: 0,
        marginTop:0,
        marginBottom:0,
        borderBottomWidth:5,
        borderBottomColor:'#4CAF50'
    },  
    listGolesHeader:{color:'#000',fontSize:10,textAlign:'left'},
    golesLabel:{
        color:'#fff',
        textAlign:'left',
        fontWeight: 'bold',
        fontSize:11,
        maxWidth: 85
    },
    teamLogoGoleadores:{
        width:20,height:20,margin:5
    },
    teamLogoGoleadoresCustom:{
        width:10,height:10,margin:3
    },
    flexOneCenter:{
        flex:1,
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center'
    },
    flexOne:{
        flex:1
    },
    flexCincuenta:{
        flex:0.5
    },
    flexCuarenta:{
        flex:0.4
    },
    flexDiez:{
        flex:0.1,
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center'
    },
    flexVeinte:{
        flex:0.20
    },  
    shieldLogo:{
        width:deviceWidth/3,
        margin:10,
        height:deviceWidth/3,
    },
    listPartidosContainer:{
        width: '100%', 
        marginVertical:0,
        paddingTop:0,
        paddingBottom:0,
        marginLeft: 0, 
        paddingLeft: 0,
        paddingRight: 0, 
        marginRight: 0,
        marginTop:0,
        marginBottom:0,
        borderBottomWidth:5,
        borderBottomColor:'#4CAF50'
    },
    listPartidosContainerCustom:{
        width: '100%', 
        marginVertical:0,
        paddingTop:10,
        paddingBottom:10,
        marginLeft: 0, 
        paddingLeft: 0,
        paddingRight: 0, 
        marginRight: 0,
        marginTop:0,
        marginBottom:0,
    },
    customLoginBtn:{
        marginHorizontal:20,
        marginBottom:15,
        padding:12,
        borderRadius:20,
        borderWidth:1,
        borderColor:'#000',
    },
    RegisterBtn:{
        marginHorizontal:20,
        marginBottom:15,
        padding:12,
        borderRadius:20,
        borderWidth:1,
        borderColor:'#4CAF50',
        backgroundColor:'#4CAF50'
    },
    containerCallout:{
        width:deviceWidth
    },
    view2:{
        backgroundColor:'#193f6b',
    },
    miniIcon:{
        width:52,
        height:52,
        alignSelf:'center'
    },
    miniIconList:{
        marginLeft:3,
        width:30,
        height:30
    },
    bgTabla:{
        width:deviceWidth,
        height:deviceHeight-headerHeight,
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center'
    },  
    partidoQuemado:{
        width:deviceWidth,
        height:deviceHeight-headerHeight,
    },
    tablaQuemada:{
        width:deviceWidth*4/5,
        height:deviceHeight/2,
        alignSelf:'center'
    },
    miniIconCustom:{
        width:22,
        height:22,
        marginRight:30
    },
    backgroundSize:{
        width:deviceWidth,
        height:deviceHeight,
    },
    view1:{
        backgroundColor:'#388E3C',
    },
    button:{
        backgroundColor:'#1772b6',
    },
    title:{
        fontSize:22,
        textAlign:'center',
        color:'#000'
    },
    whiteTitle:{
        fontSize:22,
        textAlign:'center',
        color:'#fff'
    },
    whiteTitleCustom:{
        fontSize:20,
        textAlign:'center',
        color:'#fff',
        fontWeight:"900"
    },
    whiteSmallTitle:{
        fontSize:16,
        color:'#fff',
        textAlign:'center'
    },
    whiteSmallTitleCustom:{
        fontSize:11,
        color:'#fff',
        textAlign:'center'
    },
    br:{
        height:30,
    },
    modal:{
        alignItems:'center',
        justifyContent:'center',
        alignContent:'center',
        width:'100%',
        height:deviceHeight
    },
    modalContainer:{
        width:'90%',
        height:deviceHeight/2,
        backgroundColor:'#fff',
        borderRadius:10,
        alignSelf:'center',
        marginTop:deviceHeight/4,
    },
    smallTitle:{
        fontSize:18,
        textAlign:'center',
        color:'#000'
    },
    cercaTitle:{
        paddingTop:0,
        margin:0,
        marginTop:0
    },
    center:{
        textAlign:'center',
    },
    mediumtitleLeft:{
        textAlign:'left',
        color:'#3F51B5',
        margin:5
    },
    smallTitleLeft:{
        fontSize:16,
        textAlign:'left',
        color:'#000',
        margin:5
    },
    smallTitleNew:{
        fontSize:16,
        textAlign:'center',
        color:'#000',
        margin:5
    },
    miniSmallTitleLeft:{
        fontSize:14,
        textAlign:'left',
        color:'#434343',
        margin:5,
    },
    mainImage:{
        width:'100%',
        height:180,
        marginVertical:20,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "stretch",
    },
    mainImageCustom:{
        width:'50%',
        height:90,
        marginVertical:20,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    mainImageFloating:{
        width:'70%',
        height:100,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "center",
    },
    thumbnail:{
        width:70,
        height:70
    },
    smallImage:{
        width:120,
        height:90,
    },
    middleImage:{
        width:'100%',
        height:180,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "stretch",
    },
    maxImage:{
        width:'100%',
        height:250,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "stretch",
    },
    mapFinder:{
        width:'100%',
        height:(deviceHeight-(headerHeight + 30))*1/2,
        marginBottom:0,
    },
    flatListFinder:{
        width:'100%',
        height:(deviceHeight-(headerHeight+30))/3
    },

});