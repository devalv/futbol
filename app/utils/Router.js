import React,{Component} from 'react';
import { Router,Stack, Scene, Actions } from 'react-native-router-flux';
import Login from '../components/Login';
import LoginMail from '../components/LoginMail';
import First from '../components/First';
import Index from '../components/Index';
import Register from '../components/Register';
import About from '../components/About';
import Barrial from '../components/Barrial';
import Equipos from '../components/Equipos';
import Categorias from '../components/Categorias';
import Partidos from '../components/Partidos';
import AllPartidos from '../components/AllPartidos';
import Goleadores from '../components/Goleadores';
import Configuracion from '../components/Configuracion';
import DetallePartido from '../components/DetallePartido';
import DetalleJugador from '../components/DetalleJugador';

export default class RouterComponent extends Component{
render(){
    return (
        <Router>
          <Scene key="root" panHandlers={null}>
            <Scene key="partidos" component={Partidos} hideNavBar={true} initial={this.props.loggedIn?true:false}/>  
            <Scene key="first" component={First} hideNavBar={true} initial={this.props.loggedIn?false:true}/>
            <Scene key="index" component={Index} hideNavBar={true}/>  
            <Scene key="goleadores" component={Goleadores} hideNavBar={true} />    
            <Scene key="allPartidos" component={AllPartidos} hideNavBar={true} />  
            <Scene key="barrial" component={Barrial} hideNavBar={true} />
            <Scene key="teams" component={Equipos} hideNavBar={true} />
            <Scene key="categorias" component={Categorias} hideNavBar={true} />
            <Scene key="login" component={Login} hideNavBar={true} />
            <Scene key="loginMail" component={LoginMail} hideNavBar={true} />
            <Scene key="register" component={Register} hideNavBar={true}/>
            <Scene key="about" component={About} hideNavBar={true}/>
            <Scene key="detallePartido" component={DetallePartido} hideNavBar={true} />
            <Scene key="detalleJugador" component={DetalleJugador} hideNavBar={true} />
            <Scene key="goleadores" component={Goleadores} hideNavBar={true}/>
            <Scene key="configuracion" component={Configuracion} hideNavBar={true}/>
          </Scene>
        </Router>
    );
  }
}
