import firebase from 'firebase';
export default firebase.initializeApp({
    apiKey: "AIzaSyC7y7-XB3a7TZs6XD4w5IR7UWyqGDdfG7g",
    authDomain: "millenial-futbol.firebaseapp.com",
    databaseURL: "https://millenial-futbol.firebaseio.com",
    projectId: "millenial-futbol",
    storageBucket: "millenial-futbol.appspot.com",
    messagingSenderId: "811708006106"
});
import {Notifications} from 'expo';
import * as Permissions from 'expo-permissions'
import {  AsyncStorage } from 'react-native';
export let db = firebase.database();
export let auth = firebase.auth();
export const provincias = db.ref('provincias');
export const barriales = db.ref('ligas');
export const ligas = db.ref('ligas');
export const publicidad = db.ref('publicidad');
export const campeonatos = db.ref('campeonatos');

export async function getItem(item) {
    try {
      const value = await AsyncStorage.getItem(item);
      return value;
    } catch (error) {
        console.log('error getting item: ', error);
        return null;
    }
    return null;
}
export async function setItem(name,item) {
    try {
      const value = await AsyncStorage.setItem(name,item);
      return value;
    } catch (error) {
        console.log('error setting item: ', error);
        return null;
    }
    return null;
}
export async function removeItem(item) {
    try {
      const value = await AsyncStorage.removeItem(item);
      return value;
    } catch (error) {
        console.log('error: ', error);
        return null;
    }
    return null;
}
export async function logOutUser(){
    try{
        auth.signOut();
        const value = await AsyncStorage.clear()
        return value;
    }
    catch (error){
        console.log('error: ', error);
        return 'error';
    }
}
export function logInUser(email, password) {
    return auth.signInWithEmailAndPassword(email, password);
}
export function registerNewUser(email,password){
    return auth.createUserWithEmailAndPassword(email,password);
}
export function saveUser(data,uid){
    let ref = db.ref('users/'+uid);
    ref.set(data);
}
export function saveConfig(config,uid){
    let ref = db.ref('users/'+uid+'/config/');
    ref.set(config);
}
export function getUriTeam(equipo){
    getLigas().then(val=>{
        if(val.equipos){
            Object.values(elem.equipos).map(ele=>{
                if(ele.nombre===equipo){
                    return ele.escudo;
                }
            })
        }
    });
}
export function getLigas(){
    return ligas.once('value', element=>{
        return Object.values(element.toJSON());
    });
}
export function getPublicidad(){
    return publicidad.once('value', element=>{
        return Object.values(element.toJSON());
    });
}
export async function registerForPushNotificationsAsync(uid){
    let { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    if(status !== 'granted') {
        console.log('status: ', status);
        return false;
    }
    let token = await Notifications.getExpoPushTokenAsync();
    db.ref('users/'+uid+'/token/').set(token);
    return token;
};