import React,{Component} from 'react';
import { AppRegistry, View, Image,ImageBackground,TouchableOpacity, AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import { Actions } from 'react-native-router-flux';
import mini from '../../assets/logo.png';
import tabla from '../../assets/tabla.png';
import partidos from '../../assets/tutorial1.png';
import goleadores from '../../assets/tutorial2.png';
import configuracion from '../../assets/settings.png';
import question from '../../assets/question.png';
import bgTop from '../../assets/topDrawer.png';
import bgBottom from '../../assets/bgDrawer.png';
import {getPublicidad} from '../utils/Controller';
import Gallery from 'react-native-image-gallery';

var style = require('../utils/Styles');

export default class DrawerMenu extends Component{
    constructor(props) {
        super(props);
        this.state = { 
            error: '', 
            isLoading: false,
            publicidad: [],
            publicidadFotos: []
        };
    }
    componentWillMount() {
        let self = this;
        getPublicidad().then(val=>{
            let publicidad = Object.values(val.toJSON());
            const publicidadFotos = publicidad.filter(obj => obj.drawer === true).map(obj => {
                return {
                    source:{
                        uri: obj.foto
                    }
                }
            });
            self.setState({publicidad, publicidadFotos});
          }).catch(err=>{
            console.log('err: ', err);
          })
    }
    render(){
        return (
            <View>
                <Header>
                    <Left style={{flex:1}}></Left>
                    <Body style={{flex:1}}>
                        <Image resizeMode="contain" style={style.miniIcon} source={mini} />
                    </Body>
                    <Right style={{flex:1}}>
                    <Button transparent onPress={this.props.closeDrawer}>
                        <Icon style={{color:"#fff"}} name="md-close" />
                    </Button>
                    </Right>
                </Header>
                <View style={style.drawerContainer}>
                    <Image source={bgTop} style={style.drawerTop} resizeMode="cover" ></Image>
                    <ImageBackground source={bgBottom} style={style.drawerBottom} resizeMode="cover" >
                        <TouchableOpacity onPress={()=>{Actions.partidos();this.props.closeDrawer();}}>
                            <View style={style.drawerItemContainer}>
                                <Image source={partidos} resizeMode='contain' style={style.miniIconList}></Image>
                                <Text style={style.drawerTextOptions}>Partidos</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{Actions.index();this.props.closeDrawer();}}>
                            <View style={style.drawerItemContainer}>
                                <Image source={tabla} resizeMode='contain' style={style.miniIconList}></Image>
                                <Text style={style.drawerTextOptions}>Tablas</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{Actions.goleadores();this.props.closeDrawer();}}>
                            <View style={style.drawerItemContainer}>
                                <Image source={goleadores} resizeMode='contain' style={style.miniIconList}></Image>
                                <Text style={style.drawerTextOptions}>Goleadores</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{Actions.first({fromConfig:true});this.props.closeDrawer();}}>
                            <View style={style.drawerItemContainer}>
                                <Image source={configuracion} resizeMode='contain' style={style.miniIconList}></Image>
                                <Text style={style.drawerTextOptions}>Configuración</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{Actions.about();this.props.closeDrawer();}}>
                            <View style={style.drawerItemContainer}>
                                <Image source={question} resizeMode='contain' style={style.miniIconList}></Image>
                                <Text style={style.drawerTextOptions}>Acerca de</Text>
                            </View>
                        </TouchableOpacity>
                        <Gallery
                            style={{ flex: 1, backgroundColor: 'black' }}
                            initialPage="1"
                            //initial image to show
                            images={this.state.publicidadFotos}
                        />
                    </ImageBackground>
                </View>
            </View>
        );
    }
}
