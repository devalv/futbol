import React,{Component} from 'react';
import {  Dimensions, PixelRatio, AppRegistry,Modal, View, ImageBackground,Image,TouchableHighlight, AsyncStorage,FlatList ,Alert, WebView, Linking,ScrollView, Platform } from 'react-native';
import { Container, Label,Header, Radio,Toast, Input,Item,Form,Thumbnail, Content, Card, CardItem, Button, Left, Right, Picker,Body, Icon,List,ListItem, Text, Spinner, Title, Footer, FooterTab } from 'native-base';
import Drawer from 'react-native-drawer';
import { Actions,  } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import Gallery from 'react-native-image-gallery';
import bgTabla from '../../assets/bgField.png';
import Moment from 'moment';
import partidos from '../../assets/tutorial1.png';
import partidoBg from '../../assets/partidoBg.jpg';
import Swiper from 'react-native-swiper'
import {getItem,setItem,logOutUser,db,campeonatos,getUriTeam, getLigas, getPublicidad} from '../utils/Controller';
import DrawerMenu from './DrawerMenu.js';
const platform = Platform.OS;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const isIphoneX = platform === "ios" && (deviceHeight === 812 || deviceWidth === 812);
const headerHeight = platform === "ios" ? (isIphoneX ? 88 : 64) : 56;

var style = require('../utils/Styles');

export default class Partidos extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        modalVisible:false,
        partidos:[],
        ligas:[],
        publicidad:[],
        publicidadFotos:[]
    };
  }
  async startOver(){
    let self = this;
        getPublicidad().then(val=>{
            let publicidad = Object.values(val.toJSON());
            const publicidadFotos = publicidad.filter(obj => obj.banner === true).map(obj => {
                return obj.foto
            });
            self.setState({publicidad, publicidadFotos});
          }).catch(err=>{
            
          })
    getLigas().then(val=>{
      let ligas = Object.values(val.toJSON());
      self.setState({ligas});
    }).catch(err=>{
      
    })
    this.setState({isLoading:true,loading:true,refreshing:true,});
    this.getUserInfo()
  } 
  logout(){
    logOutUser();
    Actions.first();
  }
  logoutModal = ()=>{
    Alert.alert(
        'Confirmación',
        '¿Está seguro de que desea salir?',
        [
          {text: 'Cancel', onPress: () => {}, style: 'cancel'},
          {text: 'Salir', onPress: () => this.logout()},
        ],
    );
  }
  closeDrawer = () => {
    this.setState({modalVisible:false});
  };
  openDrawer = () => {
    this.setState({modalVisible:true});
  };
  detailPartido = (value) => {
    Actions.detallePartido({infoPartido:JSON.stringify(value)})
  }
  renderPartidos = (item,index) =>{
    return(
      <ListItem noBorder style={style.listPartidosContainer} key={index.toString()+item.fecha.toString()} onPress={()=>this.detailPartido(item)}>
        <Body style={{paddingTop:0,paddingBottom:0,marginVertical:0}}>
            <Card style={{backgroundColor:'transparent', marginVertical:0,padding:0}}>
              <ImageBackground source={partidoBg} style={{width:'100%',flex:1,height:'auto'}} resizeMode="cover" >
                <CardItem cardBody style={{backgroundColor:'transparent',paddingTop:20,paddingBottom:20}}>
                  <Left style={{flex:1}}>
                    <Image source={{uri:item.local.escudo }} resizeMode="contain" style={style.shieldLogo}/>
                  </Left>
                  <Body style={{flex:1, justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                    <Title style={{color:'#fff',fontSize:36}}>{item.resultado?item.resultado:"VS"} </Title>
                    <Text style={{color:'#fff', fontSize:13}}>{item.fecha}</Text>
                    <Text style={{color:'#fff', fontSize:13}}>{new Date(item.fecha + ' ' +item.hora).toLocaleTimeString()}</Text>
                  </Body>
                  <Right style={{flex:1}}>
                    <Image source={{uri: item.visitante.escudo}} resizeMode="contain" style={style.shieldLogo}/>
                  </Right>
                </CardItem>
              </ImageBackground>
              <CardItem style={{backgroundColor:'transparent',paddingTop:0,paddingBottom:0,marginVertical:0}}>
                <Left style={{flex:1}}>
                  <Text style={{color:'#000',textAlign:'center'}} numberOfLines={2}>{item.local.nombre} </Text>
                </Left>
                <Right style={{flex:1}}>
                  <Text style={{color:'#000',textAlign:'center'}}  numberOfLines={2}>{item.visitante.nombre} </Text>
                </Right>
              </CardItem>
            </Card>
        </Body>
      </ListItem>
    );
  }
  componentWillMount(){
    this.getUserInfo();  
    this.startOver();
  }
  async setPartidos(equipos){
    let self = this;
    await campeonatos.on('value', async el=>{
      let partidos=[];
      let partidosKeys=[];
      await Object.values(el.toJSON()).map(async (ele)=>{ //campeonatos
        await equipos.map(async (element)=>{
          if(element.categoría===ele.categoría){
            if(ele.partidos){
              await Object.entries(ele.partidos).map(async (elem)=>{
                if(element.nombre===elem[1].local.nombre){
                  if(!partidosKeys.includes(elem[0])){
                    partidos.push(elem[1]);
                    partidosKeys.push(elem[0]);
                  }
                }
                else if(element.nombre===elem[1].visitante.nombre){
                  if(!partidosKeys.includes(elem[0])){
                    partidos.push(elem[1]);
                    partidosKeys.push(elem[0]);
                  }
                }
              });
            }
          }
        })
      });
      self.setState({
        partidos,
        isLoading:false,
      });
    });
  }
  getUserInfo =async() =>{
    let self = this;
    let uid = await getItem('uid');
    if(uid!==null){
      let userInfo = db.ref('users/'+uid+'/config/');
      userInfo.on('value', snapshot => {
        let info = snapshot.toJSON();
        let equipos = Object.values(info.equipos);
        self.setState({userInfo:info,equipos});
        self.setPartidos(equipos);
      })
    }
  }
  renderPublicidad = () => {
    const {publicidadFotos} =  this.state;
    return publicidadFotos.map((pub) => <Image source={{uri: pub}} resizeMode="contain" key={pub} style={{width: '100%', height: '100%'}} />);
  }
render(){
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#4CAF50' />
          </View>
      )
    }
    return (
      <Container>
      <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                
              }}>
              <DrawerMenu closeDrawer={this.closeDrawer}></DrawerMenu>
            </Modal>
            <Header>
              <Left style={{flex:1}}>
                <Button transparent onPress={this.openDrawer}>
                    <Icon style={{color:"#fff"}} name="md-menu" />
                </Button>
              </Left>
              <Body style={{flex:1,justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                  <Title style={{fontSize:12}}>Partidos</Title>
              </Body>
              <Right style={{flex:1}}>
                <Button transparent onPress={this.startOver.bind(this)}>
                    <Icon style={{color:"#fff"}} name="md-refresh" />
                </Button>
                <Button transparent onPress={this.logoutModal}>
                    <Icon style={{color:"#fff"}} name="md-exit" />
                </Button>
              </Right>
          </Header>
        <Content>
            <List 
              style={{margin:0,width:'100%',padding:0}}
              dataArray={this.state.partidos}
              renderRow={this.renderPartidos} 
              keyExtractor={(item, index) => item.toString()+index}
            />

        </Content>
        <Footer style={{backgroundColor: '#000'}}>
          <FooterTab style={{backgroundColor: '#000'}}>
            <Swiper
            style={{backgroundColor: 'black' }}
            showButtons={false}
            showsPagination={false}
            loop
            autoplay={true}
            >
              {this.renderPublicidad()}
            </Swiper>
          </FooterTab>
        </Footer>
      </Container>
    
    );
  }
}
