import React,{Component} from 'react';
import { AppRegistry, View, Image,TouchableHighlight, KeyboardAvoidingView,AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import Swiper from 'react-native-swiper';
import qs from 'qs';
import { Actions, ActionConst } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import logo from '../../assets/logo.png';
import facebook from '../../assets/facebook.png';
import main from '../../assets/tutorial2.png';
import {registerNewUser,saveUser,getItem,setItem,registerForPushNotificationsAsync} from '../utils/Controller';
var style = require('../utils/Styles.js');

export default class About extends Component{
  constructor(props) {
    super(props);
    this.state = { 
    };
}
sendEmail = async() => {
  const to = 'paulnaranjo@gmail.com'
  let url = `mailto:${to}`;
  const query = qs.stringify({
      subject: 'Tu Fútbol App'
  });
  if (query.length) {
      url += `?${query}`;
  }
  const canOpen = await Linking.canOpenURL(url);
  if (!canOpen) {
      throw new Error('Provided URL can not be handled');
  }
  return Linking.openURL(url);
}
call =() => {
  Linking.openURL('tel: +593 99 839 9768');
}
render(){
    return (
      <Container>
        <Header>
          <Left style={{flex:1}}>
              <Button transparent onPress={()=>Actions.pop()}>
                  <Icon style={{color:"#fff"}} name="ios-arrow-back" />
              </Button>
          </Left>
          <Body style={{flex:1}}>
              <Image resizeMode="contain" style={style.miniIcon} source={mini} />
          </Body>
          <Right style={{flex:1}}></Right>
        </Header>
        <Content style={{backgroundColor: '#3d9145'}}>
          <View style={style.mainImageContainer}>
              <Image
                  resizeMode="contain"
                  style={style.mainImage}
                  source={logo}
              />
          </View>
          <View style={{padding: 20}}>
            <Text style={{alignSelf: 'center', fontSize: 24, color: '#fff', fontWeight: 'bold'}}>Tu Fútbol App</Text>
            <View style={style.br} />
            <View style={style.br} />
            <Text style={{alignSelf: 'center', color: '#fff', fontWeight: 'bold'}}>Contáctanos: </Text>
            <View style={style.br} />
            <Text style={{color: '#fff', alignSelf: 'center'}} onPress={()=>this.sendEmail()}>paulnaranjo@gmail.com</Text>
            <View style={style.br} />
            <Text style={{color: '#fff', alignSelf: 'center'}} onPress={()=>this.call()}>+593 99-839-9768</Text>
          </View>
          
        </Content>
      </Container>
    );
  }
}
