import React,{Component} from 'react';
import { AppRegistry,Modal, View, ImageBackground,Image,TouchableHighlight, AsyncStorage,FlatList ,Alert, WebView, Linking,ScrollView, Platform } from 'react-native';
import { Container, Label,Header, Radio,Toast, Input,Item,Form,Thumbnail, Content, Card, CardItem, Button, Left, Right, Picker,Body, Icon,List,ListItem, Text, Spinner, Title } from 'native-base';
import Drawer from 'react-native-drawer';
import { Actions,  } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import bgTabla from '../../assets/bgField.png';
import Moment from 'moment';
import partidos from '../../assets/tutorial1.png';
import partidoBg from '../../assets/partidoBg.jpg';
import {getItem,setItem,logOutUser,db,campeonatos,getUriTeam, getLigas} from '../utils/Controller';
import DrawerMenu from './DrawerMenu.js';
var style = require('../utils/Styles');

export default class AllPartidos extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        modalVisible:false,
        partidos:[],
        ligas:[]
    };
  }
  async startOver(){
    let self = this;
    getLigas().then(val=>{
      let ligas = Object.values(val.toJSON());
      self.setState({ligas});
    }).catch(err=>{
      console.log('err: ', err);
    })
    this.setState({isLoading:true,loading:true,refreshing:true,});
  } 
  logout(){
    logOutUser();
    Actions.first();
  }
  logoutModal = ()=>{
    Alert.alert(
        'Confirmación',
        '¿Está seguro de que desea salir?',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Salir', onPress: () => this.logout()},
        ],
    );
  }
  closeDrawer = () => {
    this.setState({modalVisible:false});
  };
  openDrawer = () => {
    this.setState({modalVisible:true});
  };
  detailPartido = (value) => {
    Actions.detallePartido({infoPartido:JSON.stringify(value)})
  }
  renderPartidos = (item,index) =>{
    return(
      <ListItem noBorder style={style.listPartidosContainer} key={index} onPress={()=>this.detailPartido(item)}>
        <Body style={{paddingTop:0,paddingBottom:0,marginVertical:0}}>
            <Card style={{backgroundColor:'transparent', marginVertical:0,padding:0}}>
              <ImageBackground source={partidoBg} style={{width:'100%',flex:1,height:'auto'}} resizeMode="cover" >
                <CardItem cardBody style={{backgroundColor:'transparent',paddingTop:20,paddingBottom:20}}>
                  <Left style={{flex:1}}>
                    <Image source={{uri:item.local.escudo }} resizeMode="contain" style={style.shieldLogo}/>
                  </Left>
                  <Body style={{flex:1, justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                    <Title style={{color:'#fff',fontSize:36}}>{item.resultado?item.resultado:"VS"} </Title>
                    <Text style={{color:'#fff', fontSize:13}}>{item.fecha}</Text>
                    <Text style={{color:'#fff', fontSize:13}}>{new Date(item.fecha + ' ' +item.hora).toLocaleTimeString()}</Text>
                  </Body>
                  <Right style={{flex:1}}>
                    <Image source={{uri: item.visitante.escudo}} resizeMode="contain" style={style.shieldLogo}/>
                  </Right>
                </CardItem>
              </ImageBackground>
              <CardItem style={{backgroundColor:'transparent',paddingTop:0,paddingBottom:0,marginVertical:0}}>
                <Left style={{flex:1}}>
                  <Text style={{color:'#000',textAlign:'center'}} numberOfLines={2}>{item.local.nombre} </Text>
                </Left>
                <Body style={{flex:1}}>
                  
                </Body>
                <Right style={{flex:1}}>
                  <Text style={{color:'#000',textAlign:'center'}}  numberOfLines={2}>{item.visitante.nombre} </Text>
                </Right>
              </CardItem>
            </Card>
        </Body>
      </ListItem>
    );
  }
  componentWillMount(){
    if(this.props.keyCampeonato){
      this.setPartidos(this.props.keyCampeonato)
    }
  }
  async setPartidos(keyCampeonato){
    let self = this;
    await campeonatos.child(keyCampeonato).on('value', async el=>{
      const ele = el.toJSON();
      let partidos=[];
      let partidosKeys=[];
      if(ele.partidos){
        await Object.entries(ele.partidos).map((elem)=>{
            if(!partidosKeys.includes(elem[0])){
              partidos.push(elem[1]);
              partidosKeys.push(elem[0]);
            }
        });
      }
      self.setState({
        partidos,
        isLoading:false,
      });
    });
  }
render(){
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#4CAF50' />
          </View>
      )
    }
    return (
      
      <Container>
            <Header>
              <Left style={{flex:1}}>
                <Button transparent onPress={()=>Actions.pop()}>
                    <Icon style={{color:"#fff"}} name="ios-arrow-back" />
                </Button>
              </Left>
              <Body style={{flex:1,justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                  <Title style={{fontSize:12}}>Partidos</Title>
              </Body>
              <Right style={{flex:1}}>
                <Button transparent onPress={this.logoutModal}>
                    <Icon style={{color:"#fff"}} name="md-exit" />
                </Button>
              </Right>
          </Header>
        <Content>
            <List 
              style={{margin:0,width:'100%',padding:0}}
              dataArray={this.state.partidos}
              renderRow={this.renderPartidos}>
            </List>
        </Content>
      </Container>
    
    );
  }
}
