import React,{Component} from 'react';
import { AppRegistry, View, Image, AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, ListItem,List,Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import {CheckBox} from 'react-native-elements';
import { Actions, ActionConst } from 'react-native-router-flux';
import tutorial1 from '../../assets/trophy.png';
import mini from '../../assets/logo.png';
var style = require('../utils/Styles.js');
import {barriales,setItem, getItem} from '../utils/Controller';

export default class Barrial extends Component{
    constructor(props) {
        super(props);
        this.state = { 
            error: '', 
            isLoading: false,
            fromConfig: false,
            barriales:[],
            barrial: []
        };
    }
    onBarrialChange = (barrial) =>{
        this.setState({barrial})
    }
    saveBarrial = async() =>{
        await setItem('barriales',this.state.barrial.toString());
        Actions.teams({fromConfig:this.state.fromConfig});
    }
    componentWillMount() {
        let fromConfig = this.props.fromConfig;
        if(fromConfig){
            this.setState({fromConfig})
        }
    }
    componentDidMount(){
        barriales.on('value',async snapshot => {
            let provincia = await getItem('provincia');
            if(provincia!==null){
                let ligasBarriales = [];
                await Object.values(snapshot.toJSON()).map(element=>{
                    if(element.provincia === provincia)
                        ligasBarriales.push(element.nombre)
                })
                this.setState({
                    barriales: ligasBarriales
                })
            }
        })
    }
    checkElement = (ligaBarrial) =>{
        let barrial = this.state.barrial;
        if(barrial.includes(ligaBarrial)){
            let index = barrial.indexOf(ligaBarrial);
            barrial.splice(index,1);
        }
        else{
            barrial.push(ligaBarrial);
        }
        this.setState({barrial})
    }
    render(){
        let barrialesOptions = this.state.barriales.length>0 &&
        this.state.barriales.map((elem,index)=>{
            return(
                <ListItem key={index} onPress={()=>{this.checkElement(elem)}}>
                    <Image source={elem.escudo ? elem.escudo !== '' ? {uri:elem.escudo} : mini : mini}  style={{width:40,height:40}} resizeMode="contain"></Image>
                    <Body>
                        <CheckBox
                            title={elem}
                            checked={this.state.barrial.includes(elem)}
                            onPress={()=>{this.checkElement(elem)}}
                            containerStyle={{backgroundColor: 'transparent', borderWidth: 0}}
                            textStyle={{color: '#fff'}}
                            checkedColor='#039BE5'
                        />
                    </Body>
                </ListItem>
            )
        })

        if(this.state.isLoading == true){
            return(
                <View style={style.centerSpinner}>
                    <Spinner color='#fff' />
                </View>
            )
        }
        return (
                <Container style={style.view1}>
                    <Header>
                        <Left style={{flex:1}}>
                            <Button transparent onPress={()=>Actions.pop()}>
                                <Icon style={{color:"#fff"}} name="ios-arrow-back" />
                            </Button>
                        </Left>
                        <Body style={{flex:1}}>
                            <Image resizeMode="contain" style={style.miniIcon} source={mini} />
                        </Body>
                        <Right style={{flex:1}}></Right>
                    </Header>
                    <Content >
                    {
                        this.state.barriales.length>0?
                        <View style={style.textContainer}>
                            <Text style={style.whiteTitleCustom}>Tu Fútbol</Text>
                            <View style={style.mainImageContainer}>
                                <Image
                                    resizeMode="contain"
                                    style={style.mainImage}
                                    source={tutorial1}
                                />
                            </View>
                            <Text style={style.whiteSmallTitleCustom}>Escoge tus campeonatos barriales preferidos</Text>
                            <View style={style.br}></View>
                            <List>
                               {barrialesOptions}
                            </List>
                            <View style={style.br}></View>
                            {
                                this.state.barrial.length>0&&
                                <Button full block onPress={this.saveBarrial}>
                                    <Text>Siguiente</Text>
                                </Button>
                            }
                        </View>
                        :
                        <View>
                            <View style={style.textContainer}>
                                <Text style={style.whiteTitleCustom}>Tu Fútbol</Text>
                                <View style={style.mainImageContainer}>
                                    <Image
                                        resizeMode="contain"
                                        style={style.mainImageCustom}
                                        source={tutorial1}
                                    />
                                </View>
                                <Text style={style.whiteSmallTitle}>No hay ligas registradas</Text>
                                <View style={style.br}></View>
                            </View>
                        </View>
                    }
                    </Content>
                </Container>
        );
    }
}
