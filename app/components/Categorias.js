import React,{Component} from 'react';
import { AppRegistry, View,FlatList, Image, AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Accordion,ListItem,List,Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import {CheckBox} from 'react-native-elements';
import { Actions, ActionConst } from 'react-native-router-flux';
import tutorial2 from '../../assets/tutorial2.png';
import mini from '../../assets/logo.png';
var style = require('../utils/Styles.js');
import {equipos,setItem, getItem} from '../utils/Controller';

export default class Categorias extends Component{
    constructor(props) {
        super(props);
        this.state = { 
            error: '', 
            isLoading: false,
            equiposBarriales: [],
            categoriasFavoritas:[],
            categoríasArr:[],
            fromConfig:false
        };
    }
    saveTeams = async() =>{
        let categorías = this.state.categoríasArr;
        await setItem('equipos',JSON.stringify(categorías));
        if(this.state.fromConfig===true){
            Actions.partidos();
        } else{
            Actions.login();
        }
    }
    componentWillMount() {
        let fromConfig = this.props.fromConfig;
        if(fromConfig){
            this.setState({fromConfig})
        }
    }
    async componentDidMount(){
        let teams = await getItem('equipos');
        if (teams) {
            console.log('teams: ', teams);
            let equiposBarriales = JSON.parse(teams);
            this.setState({equiposBarriales});
        }
    }
    checkCategory = (categoria,equipo) =>{
        let categoriasFavoritas = this.state.categoriasFavoritas;
        let categoríasArr = this.state.categoríasArr;
        if(categoriasFavoritas.includes(categoria)){
            let index = categoriasFavoritas.indexOf(categoria);
            categoriasFavoritas.splice(index,1);
            categoríasArr.splice(index,1);
        }
        else{
            categoriasFavoritas.push(categoria);
            categoríasArr.push(equipo);
        }
        this.setState({categoriasFavoritas,categoríasArr})
    }
    
    render(){
        let self = this;
        let equiposOptions = this.state.equiposBarriales.length>0 &&
        Object.values(this.state.equiposBarriales).map((element)=>{
            let categories = Object.values(element.categorías);
            return categories.map((elem,index)=>{
                let equipo = {
                    nombre:element.nombre,
                    categoría:elem
                };
                return(
                    <ListItem key={index} onPress={()=>{self.checkCategory(element.nombre + ' ' + elem,equipo)}}>
                        <Image source={element.escudo ? element.escudo !== '' ? {uri:element.escudo} : mini : mini}  style={{width:40,height:40}} resizeMode="contain"></Image>
                        <Body style={{flex:1}}>
                            <CheckBox
                                title={element.nombre + ' ' + elem}
                                checked={self.state.categoriasFavoritas.includes(element.nombre + ' ' + elem,equipo)} 
                                onPress={()=>{self.checkCategory(element.nombre + ' ' + elem,equipo)}}
                                containerStyle={{backgroundColor: 'transparent', borderWidth: 0}}
                                textStyle={{color: '#fff'}}
                                checkedColor='#039BE5'
                            />
                        </Body>
                    </ListItem> 
                )
            })
            
        })

        if(this.state.isLoading == true){
            return(
                <View style={style.centerSpinner}>
                    <Spinner color='#fff' />
                </View>
            )
        }
        return (
                <Container style={style.view1}>
                    <Header>
                        <Left style={{flex:1}}>
                            <Button transparent onPress={()=>Actions.pop()}>
                                <Icon style={{color:"#fff"}} name="ios-arrow-back" />
                            </Button>
                        </Left>
                        <Body style={{flex:1}}>
                            <Image resizeMode="contain" style={style.miniIcon} source={mini} />
                        </Body>
                        <Right style={{flex:1}}></Right>
                    </Header>
                    <Content >
                    {
                        this.state.equiposBarriales.length>0?
                        <View>
                            <View style={style.textContainer}>
                                <Text style={style.whiteTitleCustom}>Tu fútbol</Text>
                                <View style={style.mainImageContainer}>
                                    <Image
                                        resizeMode="contain"
                                        style={style.mainImageCustom}
                                        source={tutorial2}
                                    />
                                </View>
                                <Text style={style.whiteSmallTitle}>Escoge las categorías de tus equipos favoritos</Text>
                                <View style={style.br}></View>
                            </View>
                                
                                {equiposOptions}
                                
                            <View style={style.textContainer}>
                                <View style={style.br}></View>
                                {
                                    this.state.categoriasFavoritas.length>0&&
                                    <Button full block onPress={this.saveTeams}>
                                        <Text>Siguiente</Text>
                                    </Button>
                                }
                            </View>
                        </View>
                        :
                        <View style={style.centerSpinner}>
                            <Spinner color='#4CAF50' />
                        </View>
                    }
                    </Content>
                </Container>
        );
    }
}
