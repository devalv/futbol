import React,{Component} from 'react';
import { AppRegistry,Modal, View, Image,TouchableHighlight, AsyncStorage,FlatList ,Alert, WebView, Linking,ScrollView, Platform } from 'react-native';
import { Container, Label,Header, Radio,Toast, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon,List,ListItem, Text, Spinner } from 'native-base';
import Drawer from 'react-native-drawer';
import { Actions,  } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import configuracion from '../../assets/settings.png';
import { Constants, Permissions, } from 'expo';
import {getItem,setItem,logOutUser,db} from '../utils/Controller';
import DrawerMenu from './DrawerMenu.js';
var style = require('../utils/Styles');

export default class Configuracion extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        modalVisible:false,
    };
  }
  async startOver(){
    this.setState({isLoading:true,loading:true,refreshing:true,});
  } 
  logout(){
    logOutUser();
    Actions.first();
  }
  logoutModal = ()=>{
    Alert.alert(
        'Confirmación',
        '¿Está seguro de que desea salir?',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Salir', onPress: () => this.logout()},
        ],
    );
  }
  closeDrawer = () => {
    this.setState({modalVisible:false});
  };
  openDrawer = () => {
    this.setState({modalVisible:true});
  };
  componentWillMount(){
    this.startOver();
  }
  async componentDidMount() {
    let self = this;
    let uid = await getItem('uid');
    if(uid!==null){
      let userInfo = db.ref('users/'+uid+'/config/');
      userInfo.on('value', snapshot => {
        self.setState({
          userInfo:(snapshot.toJSON()),
          isLoading:false,refreshing:false,loading:false
        })
      })
    }
  }
render(){
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#4CAF50' />
          </View>
      )
    }
    return (
      
      <Container>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                
              }}>
              <DrawerMenu closeDrawer={this.closeDrawer}></DrawerMenu>
            </Modal>
            <Header>
              <Left style={{flex:1}}>
                <Button transparent onPress={this.openDrawer}>
                    <Icon style={{color:"#fff"}} name="md-menu" />
                </Button>
              </Left>
              <Body style={{flex:1}}>
                  <Image resizeMode="contain" style={style.miniIcon} source={configuracion} />
              </Body>
              <Right style={{flex:1}}>
                <Button transparent onPress={this.logoutModal}>
                    <Icon style={{color:"#fff"}} name="md-exit" />
                </Button>
              </Right>
          </Header>
        <Content>
          <Text>Configuración</Text>
        </Content>
      </Container>
    
    );
  }
}
