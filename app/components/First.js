import React,{Component} from 'react';
import { AppRegistry, View, Image,ImageBackground, AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';
import tutorial from '../../assets/logo.png';
import mini from '../../assets/logo.png';
import backgroundFirst from '../../assets/background.png';
var style = require('../utils/Styles.js');
import {provincias,setItem} from '../utils/Controller';

export default class Second extends Component{
    constructor(props) {
        super(props);
        this.state = { 
            error: '', 
            isLoading: false,
            provincias:[],
            provincia:"PICHINCHA",
            fromConfig:false
        };
    }
    onProvinciaChange = (provincia) =>{
        this.setState({provincia})
    }
    saveProvincia = async() =>{
        await setItem('provincia',this.state.provincia);
        Actions.barrial({fromConfig:this.state.fromConfig});
    }
    componentWillMount() {
        let fromConfig = this.props.fromConfig;
        if(fromConfig){
            this.setState({fromConfig})
        }
    }
    componentDidMount(){
        provincias.on('value', snapshot => {
            this.setState({
              provincias: Object.values(snapshot.toJSON())
            })
        })
    }
    render(){
        let provinciasOptions = this.state.provincias.length>0 &&
        this.state.provincias.map((elem,index)=>{
            let element = elem.Provincia;
            return(
                <Picker.Item key={index} label={element} value={element} style={{color:'#fff'}}/>
            )
        })

        if(this.state.isLoading == true){
            return(
                <View style={style.centerSpinner}>
                    <Spinner color='#fff' />
                </View>
            )
        }
        return (
                <Container style={style.view1}>
                    <ImageBackground source={backgroundFirst} style={style.backgroundSize} resizeMode="cover">
                    {
                        this.state.fromConfig===true &&
                        <Header>
                            <Left style={{flex:1}}>
                                <Button transparent onPress={()=>Actions.pop()}>
                                    <Icon style={{color:"#fff"}} name="ios-arrow-back" />
                                </Button>
                            </Left>
                            <Body style={{flex:1,justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                                <Title style={{fontSize:12}}>Configuración</Title>
                            </Body>
                            <Right style={{flex:1}}>
                                
                            </Right>
                        </Header>
                    }
                        <Content >
                        {
                            this.state.provincias.length>0?
                            <View style={style.textContainer}>
                                <Text style={style.whiteTitleCustom}>Bienvenido a Tu Fútbol</Text>
                                <View style={style.mainImageContainer}>
                                    <Image
                                        resizeMode="contain"
                                        style={style.mainImage}
                                        source={tutorial}
                                    />
                                </View>
                                <Text style={style.whiteSmallTitle}>Para empezar, escoge tu provincia</Text>
                                <View style={style.br}></View>
                                <View >
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon style={{color:"#fff"}} name="ios-arrow-down" />}
                                        full
                                        headerBackButtonTextStyle={{ color: "#fff" }}
                                        headerTitleStyle={{ color: "#fff" }}
                                        itemStyle={{
                                            backgroundColor: "#fff",
                                            marginLeft: 0,
                                        }}
                                        placeholder="Escoge tu provincia"
                                        placeholderStyle={{ color: "#fff" }}
                                        placeholderIconColor="#fff"
                                        renderHeader={backAction =>
                                            <Header>
                                            <Left>
                                                <Button transparent onPress={backAction}>
                                                <Icon name="ios-arrow-back" style={{ color: "#fff" }} />
                                                </Button>
                                            </Left>
                                            <Body style={{ flex: 3 }}>
                                                <Title style={{ color: "#fff" }}>Tu provincia</Title>
                                            </Body>
                                            <Right />
                                            </Header>}
                                        itemTextStyle={{ color: '#000',padding:10 }}
                                        textStyle={{color:'#fff'}}
                                        selectedValue={this.state.provincia}
                                        onValueChange={this.onProvinciaChange}
                                    >
                                        {provinciasOptions}
                                    </Picker>
                                </View>
                                <View style={style.br}></View>
                                {
                                    this.state.provincia!==null&&
                                    <Button full block onPress={this.saveProvincia}>
                                        <Text>Siguiente</Text>
                                    </Button>
                                }
                            </View>
                            :
                            <View style={style.centerSpinner}>
                                <Spinner color='#4CAF50' />
                            </View>
                        }
                        {
                            this.state.fromConfig!==true&&
                            <Button block transparent style={style.paddingTextCustom} onPress={ ()=>Actions.loginMail() }>
                                <Text style={{color:'#fff'}}>Ya he escogido mis equipos</Text>
                            </Button>
                        }
                        </Content>
                    </ImageBackground>
                </Container>
        );
    }
}
