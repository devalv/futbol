import React,{Component} from 'react';
import { AppRegistry, View, Image,TouchableHighlight, KeyboardAvoidingView,AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import Swiper from 'react-native-swiper';
import { Actions, ActionConst } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import google from '../../assets/google.png';
import facebook from '../../assets/facebook.png';
import main from '../../assets/tutorial2.png';
import {registerNewUser,saveUser,getItem,setItem,registerForPushNotificationsAsync} from '../utils/Controller';
var style = require('../utils/Styles.js');

export default class Register extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        email:'',
        password:'',
        name:'',
    };
}
register = async() =>{
    this.setState({isLoading:true});
    let email = this.state.email;
    let self  = this;
    let name = this.state.name;
    let password = this.state.password;
    let provincia = await getItem('provincia');
    let barriales = await getItem('barriales');
    barriales = barriales.split(',');
    let equipos = await getItem('equipos');
    equipos=JSON.parse(equipos);
    registerNewUser(email, password).then(async data => {
        try {
            let user = {
              email,
              password,
              name,
              rol: 'client',
              uid:data.user.uid,
              config:{
                provincia:provincia,
                barriales:barriales,
                equipos:equipos
              }
            }
            saveUser(user,data.user.uid);
            await setItem('uid',data.user.uid);
            let token = registerForPushNotificationsAsync(data.user.uid);
            Actions.partidos();
        } catch (error) {
            self.setState({isLoading:false});
            Alert.alert('Error',error.message);
        }
    }).catch((error) => {
        self.setState({isLoading:false});
        Alert.alert('Error',error.message);
    });
}
render(){
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#fff' />
          </View>
      )
    }
    return (
      <Container>
        <Header>
          <Left style={{flex:1}}>
              <Button transparent onPress={()=>Actions.pop()}>
                  <Icon style={{color:"#fff"}} name="ios-arrow-back" />
              </Button>
          </Left>
          <Body style={{flex:1}}>
              <Image resizeMode="contain" style={style.miniIcon} source={mini} />
          </Body>
          <Right style={{flex:1}}></Right>
        </Header>
        <Content >
          <View style={style.mainImageContainer}>
              <Image
                  resizeMode="contain"
                  style={style.mainImage}
                  source={main}
              />
          </View>
          <KeyboardAvoidingView style={style.keyboardAvoiding} behavior="padding" enabled>
          <Form>
          <Item floatingLabel style={style.paddingTextCustom}>
            <Input  placeholder='Nombre completo' onChangeText={ (name) => this.setState({ name }) }/>
          </Item>
          <Item floatingLabel style={style.paddingTextCustom}>
            <Input autoCorrect={ false }   keyboardType='email-address'  autoCapitalize = 'none' placeholder='Correo electrónico' onChangeText={ (email) => this.setState({ email }) }/>
          </Item>
          <Item floatingLabel last style={style.paddingTextCustom}>
            <Input placeholder='Contraseña' onChangeText={ (password) => this.setState({ password }) } secureTextEntry />
          </Item>
            {  
              this.state.isLoading ?
              <Spinner size='small' color='black' />
              :
              <View>
                <Button block  style={style.paddingTextCustom} onPress={ this.register }>
                  <Text>Crear cuenta</Text>
                </Button>
              </View>
            }
        </Form>
        </KeyboardAvoidingView>
        </Content>
      </Container>
    );
  }
}
