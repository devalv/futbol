import React,{Component} from 'react';
import { AppRegistry,Modal, View, ImageBackground,Image,TouchableHighlight, AsyncStorage,FlatList ,Alert, WebView, Linking,ScrollView, Platform } from 'react-native';
import { Container, Label,Header, Radio,Toast, Input,Item,Form,Thumbnail, Content, Card, CardItem, Button, Left, Right, Picker,Body, Icon,List,ListItem, Text, Spinner, Title, Separator } from 'native-base';
import Drawer from 'react-native-drawer';
import { Actions,  } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import bgTabla from '../../assets/bgField.png';
import Moment from 'moment';
import partidos from '../../assets/tutorial1.png';
import partidoBg from '../../assets/partidoBg.jpg';
import {getItem,setItem,logOutUser,db,campeonatos,getUriTeam, getLigas} from '../utils/Controller';
import DrawerMenu from './DrawerMenu.js';
var style = require('../utils/Styles');

export default class DetalleJugador extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        jugador:null
    };
  }
  componentWillMount(){
    let jugador = JSON.parse(this.props.jugador);
    this.setState({jugador});
  }
render(){
    let item = this.state.jugador;
    let categorías = item.categorías!==null&&Object.values(item.categorías).map((element,index)=>{
        return(
            <ListItem key={index}>
                <Text> {element}</Text>
            </ListItem>
        )
    });
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#4CAF50' />
          </View>
      )
    }
    return (
      <Container>
            <Header>
                <Left style={{flex:1}}>
                    <Button transparent onPress={()=>Actions.pop()}>
                        <Icon style={{color:"#fff"}} name="ios-arrow-back" />
                    </Button>
                </Left>
                <Body style={{flex:1,justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                    <Title>Jugador</Title>
                </Body>
                <Right style={{flex:1}}>
                    <Button transparent onPress={this.logoutModal}>
                        <Icon style={{color:"#fff"}} name="md-exit" />
                    </Button>
                </Right>
            </Header>
        <ScrollView style={{width:'100%',marginLeft:0,marginTop:0,marginRight:0,marginBottom:0,paddingTop:0,paddingLeft:0,paddingRight:0,paddingBottom:0}}>
        {
            item!==null&&
            <List style={{margin:0,width:'100%',padding:0}}>
                <ListItem noBorder style={style.listPartidosContainer}>
                    <Body style={{paddingTop:0,paddingBottom:0,marginVertical:0,justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                        <Image source={{uri:item.foto }} style={style.shieldLogo}/>
                    </Body>
                </ListItem>
                <Separator bordered style={style.inlineBlockCustom}>
                    <Text>Datos</Text>
                </Separator>
                <ListItem>
                    <Text>{item.nombre}</Text>
                </ListItem>
                <Separator bordered style={style.inlineBlockCustom}>
                    <Text>Número</Text>
                </Separator>
                <ListItem>
                    <Text>{item.número}</Text>
                </ListItem>
                <Separator bordered style={style.inlineBlockCustom}>
                    <Text>Fecha de nacimiento</Text>
                </Separator>
                <ListItem>
                    <Text>{item.fechaDeNacimiento}</Text>
                </ListItem>
                <Separator bordered style={style.inlineBlockCustom}>
                    <Text>Categorías</Text>
                </Separator>
                {categorías}
                <Separator bordered style={style.inlineBlockCustom}>
                    <Text>Notas</Text>
                </Separator>
                <ListItem>
                    <Text> {item.notas}</Text>
                </ListItem>
            </List>
        }
        </ScrollView>
        
      </Container>
    
    );
  }
}
