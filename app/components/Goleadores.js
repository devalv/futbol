import React,{Component} from 'react';
import { AppRegistry,Modal,ImageBackground, View, Image,TouchableHighlight, AsyncStorage,FlatList ,Alert, WebView, Linking,ScrollView, Platform } from 'react-native';
import { Container, Label,Header, Radio,Toast, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon,List,ListItem, Text, Spinner, Thumbnail } from 'native-base';
import Drawer from 'react-native-drawer';
import { Actions,  } from 'react-native-router-flux';
import bgTabla from '../../assets/bgField.png';
import goleadoresQuemado from '../../assets/goleadoresQuemado.png';
import {getItem,setItem,logOutUser,db,campeonatos,getUriTeam, getLigas} from '../utils/Controller';
import DrawerMenu from './DrawerMenu.js';
var style = require('../utils/Styles');

export default class Goleadores extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        modalVisible:false,
        info:[],
        userInfo:[],
        equipos:[],
        goleadores:[],
        golesPorCampeonato:[],
    };
  }
  async startOver(){
    this.setState({isLoading:true,loading:true,refreshing:true,});
  } 

  logout(){
    logOutUser();
    Actions.first();
  }
  logoutModal = ()=>{
    Alert.alert(
        'Confirmación',
        '¿Está seguro de que desea salir?',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Salir', onPress: () => this.logout()},
        ],
    );
  }
  closeDrawer = () => {
    this.setState({modalVisible:false});
  };
  openDrawer = () => {
    this.setState({modalVisible:true});
  };
  componentWillMount(){
    this.startOver();
  }
  async setGoleadores(camps){
    let self = this;
    let partidos=[];
    let todosLosGoles=[];
    await campeonatos.on('value', async el=>{
      let campeonats = Object.values(el.toJSON());
      for(var i = 0; i < campeonats.length ; i++){
        for(var j = 0; j < camps.length ; j++){
          if(camps[j].nombre===campeonats[i].nombre){
            if(campeonats[i].partidos){
              await Object.entries(campeonats[i].partidos).filter(obj => obj[1].jugado === true).map(async (partido)=>{
                if(!partidos.includes(partido)){
                  partido.campeonato = campeonats[i].nombre;
                  partidos.push(partido);
                }
              });
            }
          }
        }
      }
      for(var i = 0; i<partidos.length;i++){
        if(partidos[i][1].goles){
          var goles = Object.values(partidos[i][1].goles);
          for(var j = 0;j<goles.length;j++){
            let gol = goles[j];
            gol.campeonato = partidos[i].campeonato;
            todosLosGoles.push(gol);
          }
        }
      }
      var goleadores = [];
      var goleadoresRep = [];
      
      for(var i = 0; i<todosLosGoles.length;i++){
        if(todosLosGoles[i].número){
          var gol = {
            equipo:todosLosGoles[i].equipo.nombre,
            equipoLogo:todosLosGoles[i].equipo.escudo,
            nombre:todosLosGoles[i].número.nombre,
            número:todosLosGoles[i].número.número,
            campeonato: todosLosGoles[i].campeonato,
            goles:1,
          }
          goleadoresRep.push(gol)
        }
      }
      const golesPorCampeonato = camps.map(obj=>{
        const campeonatito = {
          nombre: obj.nombre,
          goleadores: []
        }
        return campeonatito;
      })
      console.log('goleadoresRep: ', goleadoresRep);
      for(var i = 0; i<goleadoresRep.length;i++){
        var gol = goleadoresRep[i];
        let exists = false;
        let position = -1;
        for(var j = 0;j<goleadores.length;j++){
          if(goleadores[j].equipo === gol.equipo && goleadores[j].nombre === gol.nombre && goleadores[j].número === gol.número){
            exists = true;
            position = j;
          }
        }
        if(exists === true && position > -1){ 
          let goles = goleadores[position].goles;
          goles = goles + 1;
          goleadores[position].goles = goles;
        }
        else{
          goleadores.push(gol);
        }
      }
      goleadores.sort((a,b) => (a.goles > b.goles) ? -1 : ((b.goles > a.goles) ? 1 : 0)); 
      golesPorCampeonato.forEach(golPorCampeonato=>{
        goleadores.forEach(tantos => {
          if(golPorCampeonato.nombre===tantos.campeonato) { 
            golPorCampeonato.goleadores.push(tantos);
          }
        })
      })
      console.log('golesPorCampeonato: ', golesPorCampeonato);
      
      self.setState({
        partidos,
        golesPorCampeonato,
        goleadores,
        isLoading:false,
      });
    });
  }
  renderGoles = (item,index) =>{
    if (item.número) {
      return(
        <ListItem style={style.listGolesContainer} key={index}>
          <Left style={style.flexOneCenter}>
            <Image source={{uri:item.equipoLogo}} alt="escudo" style={style.teamLogoGoleadores} resizeMode='contain'></Image>
            <Text style={style.golesLabel}>{item.equipo?item.equipo:''}</Text>   
          </Left>
          <Body style={style.flexOneCenter}>
            <Text style={style.golesLabel}>{item.nombre?item.nombre:''}</Text>   
          </Body>
          <Right style={style.flexOneCenter}>
            <Text style={style.golesLabel}>{item.goles?item.goles:''}</Text>
          </Right>
        </ListItem>
      );
    }
    return null;
  }
  async setTablas(equipos){
    let self = this;
    await campeonatos.on('value', async el=>{
      let tablas=[];
      var camps = [];
      let campeonats = Object.values(el.toJSON());
      for(var i = 0; i< campeonats.length; i++){
        for(var j = 0; j<equipos.length;j++){
          let equipes = Object.values(campeonats[i].equipos);
          for(var l = 0; l<equipes.length;l++){
            if(equipes[l].nombre === equipos[j].nombre){
              if(equipos[j].categoría===campeonats[i].categoría){      
                var exists = false;
                var position = -1;
                for(var k = 0; k<camps.length;k++){
                  if(campeonats[i]==camps[k]){
                    exists = true;
                    position = k;
                    break;
                  }
                }
                if(exists === false && position===-1){
                  camps.push(campeonats[i]);
                }
              }
            }
          }
        }
      }
      self.setGoleadores(camps);
    });
  }
  async componentDidMount() {
    let self = this;
    let uid = await getItem('uid');
    if(uid!==null){
      let userInfo = db.ref('users/'+uid+'/config/');
      userInfo.once('value', snapshot => {
        let info = snapshot.toJSON();
        let equipos = Object.values(info.equipos);
        self.setState({userInfo:info,equipos});
        self.setTablas(equipos);
      })
    }
  }
render(){
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#4CAF50' />
          </View>
      )
    }
    return (
      <Container>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                
              }}>
              <DrawerMenu closeDrawer={this.closeDrawer}></DrawerMenu>
            </Modal>
            <Header>
              <Left style={{flex:1}}>
                <Button transparent onPress={this.openDrawer}>
                    <Icon style={{color:"#fff"}} name="md-menu" />
                </Button>
              </Left>
              <Body style={{flex:1,justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                  <Title style={{fontSize:12}}>Goleadores</Title>
              </Body>
              <Right style={{flex:1}}>
                <Button transparent onPress={this.logoutModal}>
                    <Icon style={{color:"#fff"}} name="md-exit" />
                </Button>
              </Right>
          </Header>
          <ImageBackground style={style.bgTabla} source={bgTabla} resizeMode="cover">
          <ScrollView style={{width:'100%'}}>
              {
                this.state.golesPorCampeonato.length>0 &&
                this.state.golesPorCampeonato.map((element,index)=> (
                  element.goleadores.length>0 &&
                    <List style={{marginTop:30}}>
                      <ListItem itemHeader style={{backgroundColor:'#fff',height:20,paddingTop:0,paddingBottom:0}}>
                        <View style={style.flexOneCenter}>
                          <Text style={style.listGolesHeader}>{element.nombre}</Text>
                        </View>
                      </ListItem>
                      <ListItem itemHeader style={{backgroundColor:'#fff',height:20,paddingTop:0,paddingBottom:0}}>
                        <Left style={style.flexOneCenter}><Text style={style.listGolesHeader}>Equipo</Text></Left>
                        <Body style={style.flexOneCenter}><Text style={style.listGolesHeader}>Goleadores</Text></Body>
                        <Right style={style.flexOneCenter}><Text style={style.listGolesHeader}>Goles</Text></Right>
                      </ListItem>
                      <List 
                        dataArray={element.goleadores}
                        renderRow={this.renderGoles}>
                      </List>
                    </List>
                ))
              }
              </ScrollView>
          </ImageBackground>
      </Container>
    
    );
  }
}
