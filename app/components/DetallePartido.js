import React,{Component} from 'react';
import { AppRegistry,Modal, View, ImageBackground,Image,TouchableHighlight, AsyncStorage,FlatList ,Alert, WebView, Linking,ScrollView, Platform } from 'react-native';
import { Container, Label,Header, Radio,Toast, Input,Item,Form,Thumbnail, Content, Card, CardItem, Button, Left, Right, Picker,Body, Icon,List,ListItem, Text, Spinner, Title, Separator } from 'native-base';
import Drawer from 'react-native-drawer';
import { Actions,  } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import bgTabla from '../../assets/bgField.png';
import Moment from 'moment';
import partidos from '../../assets/tutorial1.png';
import partidoBg from '../../assets/partidoBg.jpg';
import {getItem,setItem,logOutUser,db,campeonatos,getUriTeam, getLigas} from '../utils/Controller';
import DrawerMenu from './DrawerMenu.js';
var style = require('../utils/Styles');

export default class DetallePartido extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        infoPartido:null,
        campeonato: ''
    };
  }
  getCampeonatoName =(campeonato) => {
    return campeonatos.child(campeonato+'/nombre/').on('value', snapshot=>{
        const campeonato = snapshot.toJSON();
        this.setState({campeonato})
    })
  }
  renderPartidos = (item,index) =>{
      console.log('item: ', item);
    return(
      <ListItem noBorder style={style.listPartidosContainer} key={index} onPress={this.detailPartido}>
        <Body style={{paddingTop:0,paddingBottom:0,marginVertical:0}}>
            <Card style={{backgroundColor:'transparent', marginVertical:0,padding:0}}>
              <ImageBackground source={partidoBg} style={{width:'100%',flex:1,height:'auto'}} resizeMode="cover" >
                <CardItem cardBody style={{backgroundColor:'transparent',paddingTop:20,paddingBottom:20}}>
                  <Left style={{flex:1}}>
                    <Image source={{uri:item.local.escudo }} style={style.shieldLogo}/>
                  </Left>
                  <Body style={{flex:1, justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                    <Title style={{color:'#fff',fontSize:36}}>{item.resultado?item.resultado:"VS"} </Title>
                    <Text style={{color:'#fff', fontSize:13}}>{item.fecha}</Text>
                    <Text style={{color:'#fff', fontSize:13}}>{new Date(item.fecha + ' ' +item.hora).toLocaleTimeString()}</Text>
                  </Body>
                  <Right style={{flex:1}}>
                    <Image source={{uri: item.visitante.escudo}} style={style.shieldLogo}/>
                  </Right>
                </CardItem>
              </ImageBackground>
              <CardItem style={{backgroundColor:'transparent',paddingTop:0,paddingBottom:0,marginVertical:0}}>
                <Left style={{flex:1}}>
                  <Text style={{color:'#000',textAlign:'center'}} numberOfLines={2}>{item.local.nombre} </Text>
                </Left>
                <Body style={{flex:1}}>
                  
                </Body>
                <Right style={{flex:1}}>
                  <Text style={{color:'#000',textAlign:'center'}}  numberOfLines={2}>{item.visitante.nombre} </Text>
                </Right>
              </CardItem>
            </Card>
        </Body>
      </ListItem>
    );
  }
  componentWillMount(){
    let infoPartido = JSON.parse(this.props.infoPartido);
    if(infoPartido){
        if(infoPartido.keyCampeonato){
            this.getCampeonatoName(infoPartido.keyCampeonato);
        }
    }
    this.setState({infoPartido});
  }
render(){
    let item = this.state.infoPartido;
    console.log('item: ', item);
    let oncelocal = item.OnceLocal&&Object.values(item.OnceLocal).map((element,index)=>{
        return(
            <ListItem style={style.listPartidosContainerCustom} key={index+JSON.stringify(element)} onPress={()=>Actions.detalleJugador({jugador:JSON.stringify(element)})}>
                <Body><Text numberOfLines={1}>{element.número + '  ' + element.nombre}</Text></Body>
                <Right><Image source={{uri:element.foto}} style={style.teamLogoGoleadores}/></Right>
            </ListItem>
        );
    })
    let oncevisitante = item.OnceVisitante&&Object.values(item.OnceVisitante).map((element,index)=>{
        return(
            <ListItem style={style.listPartidosContainerCustom} key={index+JSON.stringify(element)} onPress={()=>Actions.detalleJugador({jugador:JSON.stringify(element)})}>
                <Body><Text numberOfLines={1}>{element.número + '  ' + element.nombre}</Text></Body>
                <Right><Image source={{uri:element.foto}} style={style.teamLogoGoleadores}/></Right>
            </ListItem>
        );
    })
    let goles = item.goles&&Object.values(item.goles).map((element,index)=>{
        console.log('element: ', element);
        return(
            <List key={index+JSON.stringify(element)}>
                <Separator bordered style={style.inlineBlockCustom}>
                    <Text numberOfLines={1}>Minuto: {element.minuto}</Text>
                </Separator>
                <ListItem style={style.listPartidosContainerCustom} >
                    <Body>
                    {
                        element.número === 'Sin asignar' ? 
                            <Text numberOfLines={1}>{'Sin asignar'}</Text>
                        :
                            <Text numberOfLines={1}>{element.número.número+ '  ' +element.número.nombre}</Text>
                    }
                    </Body>
                    {
                        item.equip==="local"?
                            <Image source={{uri:element.equipo.escudo}} style={style.teamLogoGoleadores}/>
                        :
                            <Image source={{uri:element.equipo.escudo}} style={style.teamLogoGoleadores}/>
                    }
                    
                </ListItem>
            </List>
        );
    })
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#4CAF50' />
          </View>
      )
    }
    console.log('item: ', item);
    return (
      <Container>
            <Header>
                <Left style={{flex:1}}>
                    <Button transparent onPress={()=>Actions.pop()}>
                        <Icon style={{color:"#fff"}} name="ios-arrow-back" />
                    </Button>
                </Left>
                <Body style={{flex:1,justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                    <Title>Detalle</Title>
                </Body>
                <Right style={{flex:1}}>
                    <Button transparent onPress={this.logoutModal}>
                        <Icon style={{color:"#fff"}} name="md-exit" />
                    </Button>
                </Right>
            </Header>
        <ScrollView style={{width:'100%',marginLeft:0,marginTop:0,marginRight:0,marginBottom:0,paddingTop:0,paddingLeft:0,paddingRight:0,paddingBottom:0}}>
        {
            item!==null&&
            <List style={{margin:0,width:'100%',padding:0}}>
                <ListItem noBorder style={style.listPartidosContainer}>
                    <Body style={{paddingTop:0,paddingBottom:0,marginVertical:0}}>
                        <Card style={{backgroundColor:'transparent', marginVertical:0,padding:0}}>
                        <ImageBackground source={partidoBg} style={{width:'100%',flex:1,height:'auto'}} resizeMode="cover" >
                            <CardItem cardBody style={{backgroundColor:'transparent',paddingTop:20,paddingBottom:20}}>
                            <Left style={{flex:1}}>
                                <Image source={{uri:item.local.escudo }} style={style.shieldLogo} resizeMode="contain"/>
                            </Left>
                            <Body style={{flex:1, justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                                <Title style={{color:'#fff',fontSize:36}}>{item.resultado?item.resultado:"VS"} </Title>
                                <Text style={{color:'#fff', fontSize:13}}>{item.fecha}</Text>
                                <Text style={{color:'#fff', fontSize:13}}>{new Date(item.fecha + ' ' +item.hora).toLocaleTimeString()}</Text>
                            </Body>
                            <Right style={{flex:1}}>
                                <Image source={{uri: item.visitante.escudo}} style={style.shieldLogo}  resizeMode="contain"/>
                            </Right>
                            </CardItem>
                        </ImageBackground>
                        <CardItem style={{backgroundColor:'transparent',paddingTop:0,paddingBottom:0,marginVertical:0}}>
                            <Left style={{flex:1}}>
                            <Text style={{color:'#000',textAlign:'center'}} numberOfLines={2}>{item.local.nombre} </Text>
                            </Left>
                            <Right style={{flex:1}}>
                            <Text style={{color:'#000',textAlign:'center'}}  numberOfLines={2}>{item.visitante.nombre} </Text>
                            </Right>
                        </CardItem>
                        </Card>
                    </Body>
                </ListItem>
                <Separator bordered style={style.inlineBlockCustom}>
                    <Text>Información</Text>
                </Separator>
                {
                    this.state.campeonato!=='' &&
                    <ListItem onPress={()=>Actions.allPartidos({keyCampeonato: item.keyCampeonato})}>
                        <Text>{this.state.campeonato}</Text>
                    </ListItem>
                }
                { item.info !== ''&&
                    <ListItem>
                        <Text>{item.info.toString()}</Text>
                    </ListItem>
                }
                {
                    item.local.barrial &&
                    <ListItem>
                        <Text>{item.local.barrial}</Text>
                    </ListItem>
                }
                {
                    item.categoría &&
                    <ListItem>
                        <Text>{item.categoría}</Text>
                    </ListItem>
                }
                <Separator bordered style={style.inlineBlockCustom}>
                    <Text>Goles</Text>
                </Separator>
                {goles}
                <Separator bordered style={style.inlineBlockCustom}>
                    <Text style={{textAlign:'left'}}>Alineación {item.local.nombre}</Text>
                    <Image source={{uri:item.local.escudo }} style={style.teamLogoGoleadoresCustom}/>
                </Separator>
                {oncelocal}
                <Separator bordered style={style.inlineBlockCustom}>
                    <Text style={{textAlign:'left'}}>Alineación {item.visitante.nombre}</Text>
                    <Image source={{uri:item.visitante.escudo }} style={style.teamLogoGoleadoresCustom}/>
                </Separator>
                {oncevisitante}
            </List>
        }
        </ScrollView>
        
      </Container>
    
    );
  }
}
