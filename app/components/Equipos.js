import React,{Component} from 'react';
import { AppRegistry, View, Image, AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, ListItem,List,Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import {CheckBox} from 'react-native-elements';
import { Actions, ActionConst } from 'react-native-router-flux';
import tutorial2 from '../../assets/kick.png';
import mini from '../../assets/logo.png';
var style = require('../utils/Styles.js');
import {ligas,setItem, getItem} from '../utils/Controller';

export default class Equipos extends Component{
    constructor(props) {
        super(props);
        this.state = { 
            error: '', 
            isLoading: false,
            equiposBarriales: [],
            equiposFavoritos:[],
            fromConfig:false
        };
    }
    saveTeams = async() =>{
        await setItem('equipos',JSON.stringify(this.state.equiposFavoritos));
        Actions.categorias({fromConfig:this.state.fromConfig});
    }
    componentWillMount() {
        let fromConfig = this.props.fromConfig;
        if(fromConfig){
            this.setState({fromConfig})
        }
    }
    async componentDidMount(){
        ligas.on('value',async snapshot => {
            let barriales = await getItem('barriales');
            if(barriales !== null){
                let equiposBarriales = [];
                let barrialesArr = barriales.split(',');
                await barrialesArr.map(async elem=>{
                    var equipos = Object.values(snapshot.toJSON());
                    equipos.map(el=>{
                        if(el.nombre===elem){
                            let teams = el.equipos ? Object.values(el.equipos):[];
                            teams.map(e=>{
                                equiposBarriales.push(e);
                            })
                        }
                    });
                })
                this.setState({equiposBarriales})
            }
        })
    }
    checkElement = (equipo) =>{
        let equiposFavoritos = this.state.equiposFavoritos;
        if(equiposFavoritos.includes(equipo)){
            let index = equiposFavoritos.indexOf(equipo);
            equiposFavoritos.splice(index,1);
        }
        else{
            equiposFavoritos.push(equipo);
        }
        this.setState({equiposFavoritos})
    }
    render(){
        let equiposOptions = this.state.equiposBarriales.length>0 &&
        this.state.equiposBarriales.map((elem,index)=>{
            return(
                <ListItem key={index} onPress={()=>{this.checkElement(elem)}}>
                    <Image source={elem.escudo ? elem.escudo !== '' ? {uri:elem.escudo} : mini : mini} style={{width:40,height:40}} resizeMode="contain"></Image>
                    <Body style={{flex:1}}>
                        <CheckBox
                            title={elem.nombre}
                            checked={this.state.equiposFavoritos.includes(elem)}
                            onPress={()=>{this.checkElement(elem)}}
                            containerStyle={{backgroundColor: 'transparent', borderWidth: 0}}
                            textStyle={{color: '#fff'}}
                            checkedColor='#039BE5'
                        />
                    </Body>
                </ListItem>
            )
        })

        if(this.state.isLoading == true){
            return(
                <View style={style.centerSpinner}>
                    <Spinner color='#fff' />
                </View>
            )
        }
        return (
                <Container style={style.view1}>
                    <Header>
                        <Left style={{flex:1}}>
                            <Button transparent onPress={()=>Actions.pop()}>
                                <Icon style={{color:"#fff"}} name="ios-arrow-back" />
                            </Button>
                        </Left>
                        <Body style={{flex:1}}>
                            <Image resizeMode="contain" style={style.miniIcon} source={mini} />
                        </Body>
                        <Right style={{flex:1}}></Right>
                    </Header>
                    <Content >
                    {
                        this.state.equiposBarriales.length>0?
                        <View>
                            <View style={style.textContainer}>
                                <Text style={style.whiteTitleCustom}>Tu Fútbol</Text>
                                <View style={style.mainImageContainer}>
                                    <Image
                                        resizeMode="contain"
                                        style={style.mainImageCustom}
                                        source={tutorial2}
                                    />
                                </View>
                                <Text style={style.whiteSmallTitle}>Escoge tus equipos favoritos</Text>
                                <View style={style.br}></View>
                            </View>
                                <List>
                                {equiposOptions}
                                </List>
                            <View style={style.textContainer}>
                                <View style={style.br}></View>
                                {
                                    this.state.equiposFavoritos.length>0&&
                                    <Button full block onPress={this.saveTeams}>
                                        <Text>Siguiente</Text>
                                    </Button>
                                }
                            </View>
                        </View>
                        :
                        <View>
                            <View style={style.textContainer}>
                                <Text style={style.whiteTitleCustom}>Tu Fútbol</Text>
                                <View style={style.mainImageContainer}>
                                    <Image
                                        resizeMode="contain"
                                        style={style.mainImageCustom}
                                        source={tutorial2}
                                    />
                                </View>
                                <Text style={style.whiteSmallTitle}>No hay equipos registrados</Text>
                                <View style={style.br}></View>
                            </View>
                        </View>
                    }
                    </Content>
                </Container>
        );
    }
}
