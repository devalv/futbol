import React,{Component} from 'react';
import { AppRegistry,Modal,ImageBackground, View, Image,TouchableHighlight, AsyncStorage,FlatList ,Alert, WebView, Linking,ScrollView, Platform } from 'react-native';
import { Container, Label,Header, Radio,Toast, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon,List,ListItem, Text, Spinner, Thumbnail } from 'native-base';
import { Actions,  } from 'react-native-router-flux';
import bgTabla from '../../assets/bgField.png';
import {getItem,setItem,logOutUser,db,campeonatos,getUriTeam, getLigas} from '../utils/Controller';
import DrawerMenu from './DrawerMenu.js';
var style = require('../utils/Styles');

export default class Goleadores extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        modalVisible:false,
        info:[],
        userInfo:[],
        equipos:[],
        tablas:[],
        camps:[],
    };
  }
  async startOver(){
    this.setState({isLoading:true,loading:true,refreshing:true,});
  } 

  logout(){
    logOutUser();
    Actions.first();
  }
  logoutModal = ()=>{
    Alert.alert(
        'Confirmación',
        '¿Está seguro de que desea salir?',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Salir', onPress: () => this.logout()},
        ],
    );
  }
  closeDrawer = () => {
    this.setState({modalVisible:false});
  };
  openDrawer = () => {
    this.setState({modalVisible:true});
  };
  componentWillMount(){
    this.startOver();
  }
  async setTablas(equipos){
    let self = this;
    await campeonatos.on('value', async el=>{
      let tablas=[];
      var camps = [];
      let campeonats = Object.values(el.toJSON());
      for(var i = 0; i< campeonats.length; i++){
        for(var j = 0; j<equipos.length;j++){
          let equipes = Object.values(campeonats[i].equipos);
          for(var l = 0; l<equipes.length;l++){
            if(equipes[l].nombre === equipos[j].nombre){
              if(equipos[j].categoría===campeonats[i].categoría){      
                var exists = false;
                var position = -1;
                for(var k = 0; k<camps.length;k++){
                  if(campeonats[i]==camps[k]){
                    exists = true;
                    position = k;
                    break;
                  }
                }
                if(exists === false && position===-1){
                  camps.push(campeonats[i]);
                }
              }
            }
          }
        }
      }
      camps.map(element=>{
        var equiposT = [];
        var equiposArr = element.equipos?Object.values(element.equipos):[];
        var partidosArr = element.partidos?Object.values(element.partidos):[];
        equiposArr.map(value=>{
          var equipo = {
            nombre:value.nombre,
            escudo:value.escudo,
            categoría:element.categoría,
          }
          equiposT.push(equipo)
        })
        var currentCamp = {
          campeonato:element,
          results:[],
        };
        var results = [];
        for(var i = 0; i<equiposT.length;i++){
          var equipo = equiposT[i];
          var valoresEquipo = {
            equipo,
            jugados:0,
            ganados:0,
            perdidos:0,
            empatados:0,
            golFavor:0,
            golContra:0,
            golDiferencia:0,
            puntos:0
          }
          for(var j = 0;j<partidosArr.length;j++){
            var points = 0; //0 if lost     1 if empate    3 if win
            let partido = partidosArr[j];
            if(partido.jugado===true){
              let resultString = partido.resultado.replace('-',',');
              let resultArr = resultString.split(',');
              let resultLocal = Number(resultArr[0]);
              let resultVisitante = Number(resultArr[1]);
              if(partido.local.nombre==equipo.nombre){
                if(partido.sancionado){
                  console.log('partido.sancionado local: ', partido.sancionado);
                  if(partido.sancionado.equipo==="ambos" || partido.sancionado.equipo==="local"){
                    valoresEquipo.perdidos = valoresEquipo.perdidos + 1;
                    points = 0;
                  }  else{
                    if(resultLocal>resultVisitante){
                      points = 3;
                      valoresEquipo.ganados++;
                      valoresEquipo.golFavor += resultLocal;
                      valoresEquipo.golDiferencia += resultLocal;
                      valoresEquipo.golContra += resultVisitante;
                      valoresEquipo.golDiferencia -= resultVisitante;
                    }
                    else if(resultLocal == resultVisitante) {
                        points = 1;
                        valoresEquipo.empatados++;
                        valoresEquipo.golFavor += resultLocal;
                        valoresEquipo.golDiferencia += resultLocal;
                        valoresEquipo.golContra += resultVisitante;
                        valoresEquipo.golDiferencia -= resultVisitante;
                    }
                    else if(resultLocal<resultVisitante) {
                        points = 0;
                        valoresEquipo.perdidos++;
                        valoresEquipo.golFavor += resultLocal;
                        valoresEquipo.golDiferencia += resultLocal;
                        valoresEquipo.golContra += resultVisitante;
                        valoresEquipo.golDiferencia -= resultVisitante;
                    }
                  }
                }
                else{
                  if(resultLocal>resultVisitante){
                    points = 3;
                    valoresEquipo.ganados++;
                    valoresEquipo.golFavor += resultLocal;
                    valoresEquipo.golDiferencia += resultLocal;
                    valoresEquipo.golContra += resultVisitante;
                    valoresEquipo.golDiferencia -= resultVisitante;
                  }
                  else if(resultLocal == resultVisitante) {
                      points = 1;
                      valoresEquipo.empatados++;
                      valoresEquipo.golFavor += resultLocal;
                      valoresEquipo.golDiferencia += resultLocal;
                      valoresEquipo.golContra += resultVisitante;
                      valoresEquipo.golDiferencia -= resultVisitante;
                  }
                  else if(resultLocal<resultVisitante) {
                      points = 0;
                      valoresEquipo.perdidos++;
                      valoresEquipo.golFavor += resultLocal;
                      valoresEquipo.golDiferencia += resultLocal;
                      valoresEquipo.golContra += resultVisitante;
                      valoresEquipo.golDiferencia -= resultVisitante;
                  }
                }
                valoresEquipo.puntos+= points;
                valoresEquipo.jugados ++;
              }
              else if(partido.visitante.nombre===equipo.nombre){
                if(partido.sancionado){
                  if(partido.sancionado.equipo==="ambos" || partido.sancionado.equipo==="visitante"){
                    console.log('partido',partido);
                    valoresEquipo.perdidos = valoresEquipo.perdidos + 1;
                    points = 0;
                  }
                  else {
                    if(resultLocal>resultVisitante){
                      points = 0;
                      valoresEquipo.perdidos++;
                      valoresEquipo.golFavor += resultVisitante;
                      valoresEquipo.golDiferencia += resultVisitante;
                      valoresEquipo.golContra += resultLocal;
                      valoresEquipo.golDiferencia -= resultLocal;
                    }
                    else if(resultLocal == resultVisitante) {
                      points = 1;
                      valoresEquipo.empatados++;
                      valoresEquipo.golFavor += resultVisitante;
                      valoresEquipo.golDiferencia += resultVisitante;
                      valoresEquipo.golContra += resultLocal;
                      valoresEquipo.golDiferencia -= resultLocal;
                    }
                    else if(resultLocal<resultVisitante) {
                      points = 3;
                      valoresEquipo.ganados++;
                      valoresEquipo.golFavor += resultVisitante;
                      valoresEquipo.golDiferencia += resultVisitante;
                      valoresEquipo.golContra += resultLocal;
                      valoresEquipo.golDiferencia -= resultLocal;
                    }
                  }
                }
                else{
                  if(resultLocal>resultVisitante){
                    points = 0;
                    valoresEquipo.perdidos++;
                    valoresEquipo.golFavor += resultVisitante;
                    valoresEquipo.golDiferencia += resultVisitante;
                    valoresEquipo.golContra += resultLocal;
                    valoresEquipo.golDiferencia -= resultLocal;
                  }
                  else if(resultLocal == resultVisitante) {
                    points = 1;
                    valoresEquipo.empatados++;
                    valoresEquipo.golDiferencia += resultVisitante;
                    valoresEquipo.golFavor += resultVisitante;
                    valoresEquipo.golContra += resultLocal;
                    valoresEquipo.golDiferencia -= resultLocal;
                  }
                  else if(resultLocal<resultVisitante) {
                    points = 3;
                    valoresEquipo.ganados++;
                    valoresEquipo.golFavor += resultVisitante;
                    valoresEquipo.golDiferencia += resultVisitante;
                    valoresEquipo.golContra += resultLocal;
                    valoresEquipo.golDiferencia -= resultLocal;
                  }
                }
                valoresEquipo.puntos += points;
                valoresEquipo.jugados ++;
              }
            }
          }
          results.push(valoresEquipo);
          results.sort((a,b) => (a.golDiferencia > b.golDiferencia) ? -1 : ((b.golDiferencia > a.golDiferencia) ? 1 : 0)); 
          results.sort((a,b) => (a.puntos > b.puntos) ? -1 : ((b.puntos > a.puntos) ? 1 : 0)); 
        }
        currentCamp.results= results;
        tablas.push(currentCamp);
      });
      self.setState({
        camps,
        tablas,
        isLoading:false,
      });
    });
  }
  renderPositions = (item,index) =>{
    return(
      <ListItem style={style.listGolesContainer} key={index}>
        <Left style={style.flexCuarenta}>
          <Image source={{uri:item.equipo.escudo}} alt="escudo" style={style.teamLogoGoleadores} resizeMode='contain'></Image>
          <Text numberOfLines={3} ellipsizeMode="head" style={style.golesLabel}>{item.equipo.nombre}</Text>   
        </Left>
        <Right style={style.flexDiez}>
          <Text style={style.golesLabel}>{item.jugados}</Text>   
        </Right>
        <Right style={style.flexDiez}>
          <Text style={style.golesLabel}>{item.ganados}</Text>   
        </Right>
        <Right style={style.flexDiez}>
          <Text style={style.golesLabel}>{item.empatados}</Text>   
        </Right>
        <Right style={style.flexDiez}>
          <Text style={style.golesLabel}>{item.perdidos}</Text>   
        </Right>
        <Right style={style.flexDiez}>
          <Text style={style.golesLabel}>{item.golFavor}</Text>   
        </Right>
        <Right style={style.flexDiez}>
          <Text style={style.golesLabel}>{item.golContra}</Text>   
        </Right>
        <Right style={style.flexDiez}>
          <Text style={style.golesLabel}>{item.golDiferencia}</Text>   
        </Right>
        <Right style={style.flexDiez}>
          <Text style={style.golesLabel}>{item.puntos}</Text>   
        </Right>
      </ListItem>
    );
  }
  mounted= async()=>{
    let self = this;
    let uid = await getItem('uid');
    if(uid!==null){
      let userInfo = db.ref('users/'+uid+'/config/');
      userInfo.on('value', snapshot => {
        let info = snapshot.toJSON();
        let equipos = Object.values(info.equipos);
        self.setState({userInfo:info,equipos});
        self.setTablas(equipos);
      })
    }
  }
  refresh = () => {
    this.startOver();
    this.mounted();
  }
  componentDidMount() {
   this.mounted()
  }
render(){
  var tablasList = this.state.tablas.length>0 && this.state.tablas.map((element,index)=>{
    return(
      <List style={{marginVertical:30,width:'100%',padding:0}} key={index}>
        <ListItem itemHeader style={{backgroundColor:'#fff',height:20,paddingTop:0,paddingBottom:0}}>
          <Body style={style.flexOneCenter}><Text style={style.listGolesHeader}>{element.campeonato.nombre}</Text></Body>
        </ListItem>
        <ListItem itemHeader style={{backgroundColor:'#fff',height:20,paddingTop:0,paddingBottom:0}}>
          <Left style={style.flexCuarenta}><Text style={style.listGolesHeader}>Equipo</Text></Left>
          <Right style={style.flexDiez}><Text style={style.listGolesHeader}>PJ</Text></Right>
          <Right style={style.flexDiez}><Text style={style.listGolesHeader}>PG</Text></Right>
          <Right style={style.flexDiez}><Text style={style.listGolesHeader}>PE</Text></Right>
          <Right style={style.flexDiez}><Text style={style.listGolesHeader}>PP</Text></Right>
          <Right style={style.flexDiez}><Text style={style.listGolesHeader}>GF</Text></Right>
          <Right style={style.flexDiez}><Text style={style.listGolesHeader}>GC</Text></Right>
          <Right style={style.flexDiez}><Text style={style.listGolesHeader}>DG</Text></Right>
          <Right style={style.flexDiez}><Text style={style.listGolesHeader}>PTS</Text></Right>
        </ListItem>
        <List 
          dataArray={element.results}
          renderRow={this.renderPositions}
          style={{width:'100%',margin:0,padding:0}}>
        </List>
      </List>
    );
  })
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#4CAF50' />
          </View>
      )
    }
    return (
      <Container>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                
              }}>
              <DrawerMenu closeDrawer={this.closeDrawer}></DrawerMenu>
            </Modal>
            <Header>
              <Left style={{flex:1}}>
                <Button transparent onPress={this.openDrawer}>
                    <Icon style={{color:"#fff"}} name="md-menu" />
                </Button>
              </Left>
              <Body style={{flex:1,justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                  <Title style={{fontSize:12}}>Tablas</Title>
              </Body>
              <Right style={{flex:1}}>
                <Button transparent onPress={this.refresh}>
                    <Icon style={{color:"#fff"}} name="md-refresh" />
                </Button>
                <Button transparent onPress={this.logoutModal}>
                    <Icon style={{color:"#fff"}} name="md-exit" />
                </Button>
              </Right>
          </Header>
        
          <ImageBackground style={style.bgTabla} source={bgTabla} resizeMode="cover">
            <ScrollView style={{width:'100%',marginLeft:0,marginTop:0,marginRight:0,marginBottom:0,paddingTop:0,paddingLeft:0,paddingRight:0,paddingBottom:0}}>
              {tablasList}
            </ScrollView>
          </ImageBackground>
      </Container>
    
    );
  }
}
