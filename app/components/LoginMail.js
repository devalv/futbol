import React,{Component} from 'react';
import { AppRegistry, View, Image,TouchableHighlight, KeyboardAvoidingView,AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';
import {logInUser,saveConfig,getItem, setItem,registerForPushNotificationsAsync} from '../utils/Controller';
import mini from '../../assets/logo.png';
import main from '../../assets/kick.png';
var style = require('../utils/Styles.js');

export default class Register extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        email:'',
        password:'',
    };
}
toRegister = () =>{
  Actions.register();
}
login = async() =>{
    this.setState({isLoading:true});
    let email = this.state.email;
    let self  = this;
    let password = this.state.password;
    let provincia = await getItem('provincia');
    let barriales = await getItem('barriales');
    let equipos = await getItem('equipos');
    logInUser(email, password).then(async data => {
        try {
            if(provincia!==null && barriales!==null && equipos!==null){
                equipos=JSON.parse(equipos);
                barriales = barriales.split(',');
                let config = {
                      provincia:provincia,
                      barriales:barriales,
                      equipos:equipos
                  }
                  saveConfig(config,data.user.uid);
                  await setItem('uid',data.user.uid);
                  let token = registerForPushNotificationsAsync(data.user.uid);
                  Actions.partidos();
            }
            else{
                await setItem('uid',data.user.uid);
                let token = registerForPushNotificationsAsync(data.user.uid);
                Actions.partidos();
            }
            
        } catch (error) {
            self.setState({isLoading:false});
        }
       
    }).catch((error) => {
        self.setState({isLoading:false});
        Alert.alert('Error',error.message);
    });
}
render(){
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#4CAF50' />
          </View>
      )
    }
    return (
      <Container>
        <Header>
            <Left style={{flex:1}}>
                <Button transparent onPress={()=>Actions.pop()}>
                    <Icon style={{color:"#fff"}} name="ios-arrow-back" />
                </Button>
            </Left>
            <Body style={{flex:1}}>
                <Image resizeMode="contain" style={style.miniIcon} source={mini} />
            </Body>
            <Right style={{flex:1}}></Right>
        </Header>
        <Content >
          <View style={style.customImageContainerV2}>
              <Image
                  resizeMode="contain"
                  style={style.mainImageCustom}
                  source={main}
              />
          </View>
          <KeyboardAvoidingView style={style.keyboardAvoiding} behavior="padding" enabled>
          <Form>
            <Item success stackedLabel style={style.paddingTextCustom}>
                <Label>Correo electrónico</Label>
                <Input autoCorrect={ false }   keyboardType='email-address'  autoCapitalize = 'none'  onChangeText={ (email) => this.setState({ email }) }/>
            </Item>
            <Item success stackedLabel style={style.paddingTextCustom}>
                <Label>Contraseña</Label>
                <Input onChangeText={ (password) => this.setState({ password }) } secureTextEntry />
            </Item>
                {  
                this.state.isLoading ?
                <Spinner size='small' color='black' />
                :
                <View>
                    <Button block  style={style.paddingTextCustom} onPress={ this.login }>
                        <Text>Iniciar Sesión</Text>
                    </Button>
                    <Button block transparent style={style.paddingTextCustom} onPress={ this.toRegister }>
                        <Text>¿No tienes una cuenta? Regístrate.</Text>
                    </Button>
                </View>
                }
            </Form>
        </KeyboardAvoidingView>
        </Content>
      </Container>
    );
  }
}
