import React,{Component} from 'react';
import { AppRegistry, View, Image,TouchableHighlight, AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import main from '../../assets/field.png';
import {registerNewUser,saveUser,getItem,setItem,registerForPushNotificationsAsync} from '../utils/Controller';
import { Permissions, Notifications,Google,Facebook } from 'expo';
import {googleLogin,auth} from '../utils/Controller.js';
var style = require('../utils/Styles.js');
const FACEBOOK_APP_ID = '293949897844775';


export default class Login extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
    };
}
toLoginWithMail = () =>{
  Actions.loginMail();
}
toRegister = () =>{
  Actions.register();
}
toMain = async () =>{
  const randomMail = Math.floor(Math.random() * 100000000);
  this.setState({isLoading:true});
    let email = randomMail+'@tufutbol.net';
    let self  = this;
    let name = 'Usuario sin registrar';
    let password = '123456';
    let provincia = await getItem('provincia');
    let barriales = await getItem('barriales');
    barriales = barriales.split(',');
    let equipos = await getItem('equipos');
    equipos=JSON.parse(equipos);
    registerNewUser(email, password).then(async data => {
        try {
            let user = {
              email,
              password,
              name,
              rol: 'client',
              uid:data.user.uid,
              config:{
                provincia:provincia,
                barriales:barriales,
                equipos:equipos
              }
            }
            saveUser(user,data.user.uid);
            await setItem('uid',data.user.uid);
            let token = registerForPushNotificationsAsync(data.user.uid);
            Actions.partidos();
        } catch (error) {
            self.setState({isLoading:false});
            Alert.alert('Error',error.message);
        }
    }).catch((error) => {
        self.setState({isLoading:false});
        Alert.alert('Error',error.message);
    });
}
_handleGoogleLogin = async () => {
  try {
    const result = await Google.logInAsync({androidClientId: '660463357838-82s1pfcb7ccodve5304rdcdnh2tiqduc.apps.googleusercontent.com', iosClientId: '660463357838-d7cri61lamphgfo8q8eujph48v62k8t3.apps.googleusercontent.com', scopes: ["profile", "email"] });

    if (result.type === "success") { 
      const credential = firebase.auth.GoogleAuthProvider.credential( result.idToken, result.accessToken);
      auth.signInWithCredential(credential)
        .then(user => {console.log( user);})
        .catch(error => {console.log(error);});
      return result.accessToken;
    }
    return { cancelled: true };
  } catch (e) {
    return { error: true };
  }
};
 handleFacebookButton=async()=> {
   let self = this;
  const { type, token } = await Facebook.logInWithReadPermissionsAsync(FACEBOOK_APP_ID, {
    permissions: ['public_profile', 'email']
  });
  if (type === 'success') {
    //Firebase credential is created with the Facebook access token.
    const credential = auth.FacebookAuthProvider.credential(token);
    Actions.index();
    auth.signInAndRetrieveDataWithCredential(credential).then(value=>{
      Actions.index();
    }).catch(error => {
      self.setState({ errorMessage: error.message });
      Actions.index();
    });
  }
}
render(){
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#4CAF50' />
          </View>
      )
    }
    return (
      <Container>
        <Header>
          <Left style={{flex:1}}>
              <Button transparent onPress={()=>Actions.pop()}>
                  <Icon style={{color:"#fff"}} name="ios-arrow-back" />
              </Button>
          </Left>
          <Body style={{flex:1}}>
              <Image resizeMode="contain" style={style.miniIcon} source={mini} />
          </Body>
          <Right style={{flex:1}}></Right>
        </Header>
        <Content>
          <View style={style.mainImageContainer}>
              <Image
                  resizeMode="contain"
                  style={style.mainImageCustom}
                  source={main}
              />
          </View>
          <Text style={{marginHorizontal:50,textAlign:'center'}}>Ingresa o regístrate para poder recibir notificaciones.</Text>
          <View style={style.br}></View>
          <TouchableHighlight style={style.RegisterBtn} onPress={this.toLoginWithMail}>
              <Text style={{textAlign:'center',color:'#fff'}}>Iniciar sesión</Text>
          </TouchableHighlight>
          <Button block transparent style={style.paddingTextCustom} onPress={ this.toRegister }>
              <Text>¿No tienes una cuenta? Regístrate.</Text>
          </Button>
          <Button block transparent style={style.paddingTextCustom} onPress={ this.toMain }>
              <Text>Continuar sin iniciar sesión</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}
