const functions = require('firebase-functions');
var fetch = require('node-fetch');
const admin = require('firebase-admin');
const moment = require('moment');
admin.initializeApp(functions.config().firebase);

/*exports.sendPushNotificationPartidoComienza = functions.database.ref('partidoComienza/{id}').onCreate(event=>{
    const root = event.data.ref.root;
    var messages=[];
    var nombreEquipo = '';
    const data = event.data.toJSON();
    var tokens = [];
    return root.child('users').once('value').then((snapshot) => {
        snapshot.forEach((e) => {
            e.forEach((el)=>{
                if(el.key==='config'){
                    el.forEach((element)=>{
                        if (element.key==='equipos') {
                            element.forEach((valt)=>{
                                let valte = valt.toJSON();
                                if (valte.categoría === data.categoría) {
                                    if(valte.nombre === data.local.nombre) {
                                        nombreEquipo = data.local.nombre;
                                        var exists = false;
                                        for(var i = 0; i<tokens.length;i++){
                                            if(tokens[i]===e){
                                                exists = true;
                                            }
                                        }
                                        if(!exists){
                                            tokens.push(e);
                                        }
                                    }
                                    else if(valte.nombre === data.visitante.nombre) {
                                        nombreEquipo = data.visitante.nombre;
                                        var existsV = false;
                                        for(var j = 0; j<tokens.length;j++){
                                            if(tokens[j]===e){
                                                existsV = true;
                                            }
                                        }
                                        if(!existsV){
                                            tokens.push(e);
                                        }
                                    }
                                }
                            })
                        }
                    })
                }
            })
        })
        tokens.forEach((t)=>{
            t.forEach((el)=>{
                if(el.key === 'token'){
                    let expoToken = el.val();
                    if (expoToken) {
                        messages.push({
                            "to": expoToken,
                            "title":"Tu Fútbol",
                            "body":"Ha comenzado el partido de "+nombreEquipo
                        });
                        let options = {
                            method:'POST',
                            headers:{
                                "Accept":"application/json",
                                "Content-Type":"application/json"
                            },
                            body:JSON.stringify(messages)
                        };
                        fetch('https://exp.host/--/api/v2/push/send',options)
                        console.log('Notification: ', options);
                    }
                }
            })
        })
        admin.database().ref('partidoComienza').remove().then(() => {
            console.log("Remove succeeded.")
            return true;
        })
        .catch((error)=>{
            console.log("Remove failed: " + error.message)
        });
        return Promise.all(messages);
    }).then(messages=>{
        let options = {};
        setTimeout(()=>{
            let options = {
                method:'POST',
                headers:{
                    "Accept":"application/json",
                    "Content-Type":"application/json"
                },
                body:JSON.stringify(messages)
            };
        },2000)
        return fetch('https://exp.host/--/api/v2/push/send',options);
    })
    .catch(error=>{
        console.log('error: ', error);
        return error;
    })
});
*/

exports.addLogCampeonatos = functions.database.ref('/campeonatos/{id}').onWrite(event=>{
    const data = event.data.toJSON();
    const dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    return admin.database().ref('log/'+dateTime).set(JSON.stringify(data));
});
exports.addLogLigas = functions.database.ref('/ligas/{id}').onWrite(event=>{
    const data = event.data.toJSON();
    const dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    return admin.database().ref('log/'+dateTime).set(JSON.stringify(data));
});
exports.addLogUsers = functions.database.ref('/users/{id}').onWrite(event=>{
    const data = event.data.toJSON();
    const dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    return admin.database().ref('log/'+dateTime).set(JSON.stringify(data));
});
exports.aboutToSancionate = functions.database.ref('/campeonatos/{campeonato}/partidos/{partido}/amonestaciones/').onWrite(event=>{
    const amonestations = event.data.toJSON();
    console.log('amonestations: ', amonestations);
    return true;
});

exports.sendPushNotificationPartidoTermina = functions.database.ref('partidoTermina/{id}').onCreate(event=>{
    const root = event.data.ref.root;
    var messages=[];
    var nombreEquipo = '';
    var categoría = '';
    var resultado = '0-0';
    const data = event.data.toJSON();
    var tokens = [];
    var contig = [];
    return root.child('users').once('value').then((snapshot) => {
        snapshot.forEach((e) => {
            e.forEach((el)=>{
                if(el.key==='config'){
                    el.forEach((element)=>{
                        if (element.key==='equipos') {
                            element.forEach((valt)=>{
                                let valte = valt.toJSON();
                                let exists = false;
                                if (valte.categoría === data.categoría) {
                                    if(valte.nombre === data.local.nombre) {
                                        nombreEquipo = data.local.nombre;
                                        resultado = data.resultado;
                                        categoría = data.categoría;
                                        let eJson= JSON.parse(JSON.stringify(e));
                                        for(let i = 0; i<tokens.length;i++){
                                            if(tokens[i]===eJson.token){
                                                exists = true;
                                                break;
                                            }
                                        }
                                        if(!exists){
                                            tokens.push(e);
                                            exists = true;
                                        }
                                    }
                                    if(!exists){
                                        if(valte.nombre === data.visitante.nombre) {
                                            nombreEquipo = data.visitante.nombre;
                                            resultado = data.resultado;
                                            categoría = data.categoría;
                                            let eJson= JSON.parse(JSON.stringify(e));
                                            for(let i = 0; i<tokens.length;i++){
                                                if(tokens[i]===eJson.token){                                                    
                                                    exists = true;
                                                    break;
                                                }
                                            }
                                            if(!exists){
                                                tokens.push(e);
                                            }
                                        }
                                    }
                                }
                            })
                        }
                    })
                }
            })
        })
        var arrayTokens = JSON.parse(JSON.stringify(tokens));
        for(let i = 0; i<arrayTokens.length;i++){
            let tokenExists = false;
            for(let j = 0; j <messages.length; j++){
                let element = messages[j];
                if(element.to===arrayTokens[i].token){
                    tokenExists=true;
                    break;
                }
            }
            if(tokenExists===false){
                let notificacion = {
                    "to": arrayTokens[i].token,
                    "title":"Tu Fútbol",
                    "body":"Ha terminado el partido de "+nombreEquipo + " resultado: "+resultado
                };
                messages.push(notificacion);
            }
        }
        console.log('messages: ', messages);
        let options = {
            method:'POST',
            headers:{
                "Accept":"application/json",
                "Content-Type":"application/json"
            },
            body:JSON.stringify(messages)
        };
        fetch('https://exp.host/--/api/v2/push/send',options)
        admin.database().ref('partidoTermina').remove();
        return Promise.all(messages);
    }).then(messages=>{
        let options = {};
        setTimeout(()=>{
            let options = {
                method:'POST',
                headers:{
                    "Accept":"application/json",
                    "Content-Type":"application/json"
                },
                body:JSON.stringify(messages)
            };
        },1000)
        return fetch('https://exp.host/--/api/v2/push/send',options);
    })
    .catch(error=>{
        console.log('error: ', error);
        return error;
    })
});


/*exports.sendPushNotificationGoles = functions.database.ref('notificacionGol/{id}').onCreate(event=>{
    const root = event.data.ref.root;
    var messages=[];
    var nombreEquipo = '';
    var resultado = '0-0';
    var golEquipo='';
    const data = event.data.toJSON();
    var tokens = [];
    return root.child('users').once('value').then((snapshot) => {
        snapshot.forEach((e) => {
            e.forEach((el)=>{
                if(el.key==='config'){
                    el.forEach((element)=>{
                        if (element.key==='equipos') {
                            element.forEach((valt)=>{
                                let valte = valt.toJSON();
                                if (valte.categoría === data.editing.categoría) {
                                    if(valte.nombre === data.editing.local.nombre) {
                                        nombreEquipo = data.editing.local.nombre;
                                        resultado = data.gol.resultado;
                                        golEquipo = data.gol.equipo.nombre;
                                        var exists = false;
                                        for(var i = 0; i<tokens.length;i++){
                                            if(tokens[i]===e){
                                                exists = true;
                                            }
                                        }
                                        if(!exists){
                                            tokens.push(e);
                                        }
                                    }
                                    else if(valte.nombre === data.editing.visitante.nombre) {
                                        nombreEquipo = data.editing.local.nombre;
                                        resultado = data.gol.resultado;
                                        golEquipo = data.gol.equipo.nombre;
                                        var existsV = false;
                                        for(var j = 0; j<tokens.length;j++){
                                            if(tokens[j]===e){
                                                existsV = true;
                                            }
                                        }
                                        if(!existsV){
                                            tokens.push(e);
                                        }
                                    }
                                }
                            })
                        }
                    })
                }
            })
        })
        tokens.forEach((t)=>{
            t.forEach((el)=>{
                if(el.key === 'token'){
                    let expoToken = el.val();
                    if (expoToken) {
                        messages.push({
                            "to": expoToken,
                            "title":"Tu Fútbol",
                            "body":"Gol de "+golEquipo + " el resultado es: " + resultado
                        });
                        let options = {
                            method:'POST',
                            headers:{
                                "Accept":"application/json",
                                "Content-Type":"application/json"
                            },
                            body:JSON.stringify(messages)
                        };
                        fetch('https://exp.host/--/api/v2/push/send',options)
                        console.log('Notification: ', options);
                    }
                }
            })
        })
        admin.database().ref('notificacionGol').remove().then(() => {
            console.log("Remove succeeded.")
            return true;
        })
        .catch((error)=>{
            console.log("Remove failed: " + error.message)
        });
        return Promise.all(messages);
    }).then(messages=>{
        let options = {};
        setTimeout(()=>{
            let options = {
                method:'POST',
                headers:{
                    "Accept":"application/json",
                    "Content-Type":"application/json"
                },
                body:JSON.stringify(messages)
            };
        },2000)
        return fetch('https://exp.host/--/api/v2/push/send',options);
    })
    .catch(error=>{
        console.log('error: ', error);
        return error;
    })
});
*/

/*exports.sendPushNotification = functions.database.ref('updated/{id}').onCreate(event=>{
    const root = event.data.ref.root;
    var messages=[];
    const data = event.data.toJSON();
    console.log('data: ', data);
    return root.child('users').once('value').then((snapshot) => {
        snapshot.forEach((e) => {
            e.forEach((el)=>{
                if(el.key === 'token'){
                    let expoToken = el.val();
                    console.log('expoToken: ', expoToken);
                    if (expoToken) {
                        messages.push({
                            "to": expoToken,
                            "title":"Tu Fútbol",
                            "body":"Resultados nuevos disponibles"
                        });
                        let options = {
                            method:'POST',
                            headers:{
                                "Accept":"application/json",
                                "Content-Type":"application/json"
                            },
                            body:JSON.stringify(messages)
                        };
                        fetch('https://exp.host/--/api/v2/push/send',options)
                        console.log('Notification: ', options);
                    }
                }
            })
        })
        return Promise.all(messages);
    }).then(messages=>{
        let options = {};
        setTimeout(()=>{
            let options = {
                method:'POST',
                headers:{
                    "Accept":"application/json",
                    "Content-Type":"application/json"
                },
                body:JSON.stringify(messages)
            };
        },2000)
        return fetch('https://exp.host/--/api/v2/push/send',options);
    })
    .catch(error=>{
        console.log('error: ', error);
        return error;
    })
});*/